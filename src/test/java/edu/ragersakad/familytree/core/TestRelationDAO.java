package edu.ragersakad.familytree.core;

import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;

import edu.ragersakad.familytree.db.DAOs.IPersonDAO;
import edu.ragersakad.familytree.db.DAOs.IRelationDAO;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for Relation and RelationDAO.
 *
 * NOTES: - Possible to run with embedded Derby (no server needed) - Possible to
 * run with server and create tables
 *
 * @author Cecilia Schmidt
 */
public class TestRelationDAO {

    static IFamilyTree FamilyTree;
    static IPersonDAO Persons;
    static IRelationDAO Relations;
    final static String PU = "familytree_pu";
    final static String TEST_PU = "familytree_test_pu";

    @Before
    public void before() {
        FamilyTree = new FamilyTree(TEST_PU);
        Persons = FamilyTree.getPersonDAO();
        Relations = FamilyTree.getRelationDAO();
    }
    
    @Test
    public void testAddRelation() {
        Date birthDate = new Date();
        Person p1 = new Person("John", "Doe", Person.Gender.MALE, birthDate, birthDate);
        Person p2 = new Person("Jane", "Doe", Person.Gender.FEMALE, birthDate, birthDate);
        Persons.add(p1);
        Persons.add(p2);
        
        Relations.addRelation(p1, p2, Relation.RelationType.SPOUSE);
        
        assertTrue(Relations.getRelations(p1.getId()).size() == 1);
    }
    
    @Test
    public void testAddSpecificRelations() {
        Date birthDate = new Date();
        Person p1 = new Person("Juan", "Nadie", Person.Gender.MALE, birthDate, birthDate);
        Person p2 = new Person("Fulanita", "de Tal", Person.Gender.FEMALE, birthDate, birthDate);
        Persons.add(p1);
        Persons.add(p2);
        
        Relations.addMotherChild(p2, p1);
        
        assertTrue(Relations.getRelations(p1.getId()).size() == 1);
        Relation r = Relations.getRelation(p1.getId(), p2.getId());
        assertTrue(r.getType() == Relation.RelationType.MOTHER);
    }
    
    @Test
    public void testGetRelations() {
        Date birthDate = new Date();
        Person p1 = new Person("p1", "p1", Person.Gender.FEMALE, birthDate, birthDate);
        Person p2 = new Person("p2", "p2", Person.Gender.MALE, birthDate, birthDate);
        Person p3 = new Person("p3", "p3", Person.Gender.MALE, birthDate, birthDate);
        Persons.add(p1);
        Persons.add(p2);
        Persons.add(p3);
        Relations.addMotherChild(p1, p3);
        Relations.addFatherChild(p2, p3);
        
        List<Relation> relations = Relations.getRelations(p3.getId());
        
        assertTrue(relations.size() == 2);
    }
    
    @Test
    public void testGetRelation() {
        Date birthDate = new Date();
        Person p1 = new Person("P1", "P1", Person.Gender.FEMALE, birthDate, birthDate);
        Person p2 = new Person("P2", "P2", Person.Gender.MALE, birthDate, birthDate);
        Persons.add(p1);
        Persons.add(p2);
        
        Relations.addRelation(p1, p2, Relation.RelationType.SPOUSE);
        Relation r = Relations.getRelation(p1.getId(), p2.getId());
        
        assertTrue(r != null);
        assertTrue(r.getFirst().getId().equals(p1.getId()));
        assertTrue(r.getOther().getId().equals(p2.getId()));
    }
}
