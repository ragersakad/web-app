package edu.ragersakad.familytree.core;

import java.util.Date;

import edu.ragersakad.familytree.db.DAOs.IPersonDAO;
import edu.ragersakad.familytree.db.DAOs.IRelationDAO;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Cecilia Schmidt
 */
public class TestFamilyTree {

    static IFamilyTree FamilyTree;
    static IPersonDAO Persons;
    static IRelationDAO Relations;
    final static String TEST_PU = "familytree_test_pu";

    @Before
    public void before() {
        FamilyTree = new FamilyTree(TEST_PU);
        Persons = FamilyTree.getPersonDAO();
        Relations = FamilyTree.getRelationDAO();
    }
        
    @Test
    public void testCascading_removeRelationsOfRemovedPerson() {

        Date birthDate = new Date();
        Person p1 = new Person("p1", "p1", Person.Gender.FEMALE, birthDate, birthDate);
        Person p2 = new Person("p2", "p2", Person.Gender.MALE, birthDate, birthDate);
        Person p3 = new Person("p3", "p3", Person.Gender.MALE, birthDate, birthDate);
        Persons.add(p1);
        Persons.add(p2);
        Persons.add(p3);
        FamilyTree.createRelation(p1.getId(), p3.getId(), Relation.RelationType.MOTHER);
        FamilyTree.createRelation(p2.getId(), p3.getId(), Relation.RelationType.FATHER);
        Long p3Id = p3.getId();
        
        FamilyTree.deletePerson(p3Id);
        
        assert(Relations.getRelations(p3Id).isEmpty());
    }
}
