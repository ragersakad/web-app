package edu.ragersakad.familytree.core;

import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;

import edu.ragersakad.familytree.db.DAOs.IPersonDAO;
import org.junit.Before;
import org.junit.Test;

/**
 * Test for Person and PersonDAO.
 *
 * NOTES: - Possible to run with embedded Derby (no server needed) - Possible to
 * run with server and create tables
 *
 * @author Cecilia Schmidt
 */
public class TestPersonDAO {

    static IFamilyTree FamilyTree;
    static IPersonDAO Persons;
    final static String TEST_PU = "familytree_test_pu";

    @Before
    public void before() {
        FamilyTree = new FamilyTree(TEST_PU);
        Persons = FamilyTree.getPersonDAO();
    }
    
    @Test
    public void testAddPerson() {
        Date birthDate = new Date();
        Person p1 = new Person("John", "Doe", Person.Gender.MALE, birthDate, birthDate);
        
        Persons.add(p1);
        Person found = Persons.find(p1.getId());
        
        assertTrue(found != null && found.getId().equals(p1.getId()));
    }
    
    @Test
    public void testUpdatePerson() {
        Date birthDate = new Date();
        Person p1 = new Person("Jane", "Doe", Person.Gender.FEMALE, birthDate, birthDate);
        Persons.add(p1);
        String newName = "Jeanette";
        Person pNew = new Person(p1.getId(), newName , p1.getLastName(), p1.getGender(), p1.getBirthDate(), p1.getDeathDate());
        
        Person p2 = Persons.update(pNew);
        
        assertTrue(pNew.equals(p2) && p2.getFirstName().equals(newName));
    }
    
    @Test
    public void testRemovePerson() {
        Date birthDate = new Date();
        Person p1 = new Person("Jane", "Doe", Person.Gender.FEMALE, birthDate, birthDate);
        Persons.add(p1);
        
        Persons.remove(p1.getId());
        
        assertTrue(Persons.find(p1.getId()) == null);
    }
    
    @Test
    public void testGetByName() {
        String name = "Lolson";
        Date birthDate = new Date();
        Person p1 = new Person(name, "Nadie", Person.Gender.MALE, birthDate, birthDate);
        Person p2 = new Person("Fulanita", name, Person.Gender.FEMALE, birthDate, birthDate);
        Persons.add(p1);
        Persons.add(p2);
        
        List<Person> result = FamilyTree.getPersonDAO().getByName(name);
        
        assert(result.size() == 2);
    }
}
