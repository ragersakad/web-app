package edu.ragersakad.familytree.core;

import edu.ragersakad.familytree.api.helpers.Session;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 10/23/13
 * Time: 23:39
 *
 * Session test to see if the random token generator works correctly.
 */
public class TestSession {

    final int N = 1000;

    @Test
    // Test if N randomly generated tokens are different from each other.
    public void testNextToken(){
        List<String> tokens = new ArrayList<String>();
        for (int i = 0; i < N; i++){
            tokens.add(Session.nextSessionToken());
        }
        assert (checkAllDifferent(tokens));
    }

    private boolean checkAllDifferent(List<String> elements){

        for (int i=0; i < elements.size(); i++){
            for (int j=i+1; j < elements.size(); j++){
                if (elements.get(i).equals(elements.get(j))){
                    return false;
                }
            }
        }
        return true;
    }
}
