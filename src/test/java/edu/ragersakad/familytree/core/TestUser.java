package edu.ragersakad.familytree.core;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 10/23/13
 * Time: 23:22
 *
 * User test to check if the password encryption works correctly
 */
public class TestUser {

    private final String password = "123123";
    private final String encryptedPassword = "96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e";
    private User user;

    @Before
    public void setUpUser(){
        user = new User();
        user.setPassword(password);
    }

    @Test
    public void testSetPassword(){;
        try{
            Field f = User.class.getDeclaredField("password"); //NoSuchFieldException
            f.setAccessible(true);
            String storedPass = (String)f.get(user);
            assert (storedPass.equals(encryptedPassword));
        }catch (Exception e){
            assert (false);
        }
    }

    @Test
    public void testCheckPassword(){
        assert (user.checkPassword(password));
        assert (!user.checkPassword(encryptedPassword));
        assert (!user.checkPassword("NotTheCorrectPassword"));
    }

}
