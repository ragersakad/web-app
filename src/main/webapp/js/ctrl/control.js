// This file includes the methods necessary for the general app
// routes to work. This file depends on navigation, which adds general methods
// for the navigation to display properly.



// This function keeps track of the base url the app is in.
// If the url changes but the reslting binding in this Sammy app is
// the same, then the action shouldn't be executed and thus has to return.
// Also the function triggers a page-change event for sub controllers to bind to it.
var actualTab="";
function hasToReturn(context,newTab){
	if (actualTab == newTab){
		// The base url of the app hasn't changed. The code SHOULD return.
		return true;
	}else{
		actualTab = newTab;
		context.trigger("page-change");
		return false;
	}
}

// Create the sammy app, linked to the content div, which will be used as
// canvas for sub content.
// Inside this method are all the routes of the app (except some subroutes),
// which includes handling of some forms as well. It is initialized at the 
// end of the file.
var SammyGlobalApp = Sammy("#content", function(){

	this.use('Template');
	this.use('Storage');

	this.store('session', {type: ['local','cookie']});

	this.get("#/",function(){
		if (hasToReturn(this,"/")){return;}
		this.app.swap('');
		this.partial('content/main.template');
		clearLinks();
	});

	this.get("#/tree",function(){
		if (hasToReturn(this,"tree")){return;}
		this.app.swap('');
		this.partial('content/tree.template');
		changeSection("tree");
	});

	this.get("#/about",function(){
		if (hasToReturn(this,"about")){return;}
		this.app.swap('');
		this.partial('content/about.template');
		changeSection("about");
	});

	this.get(/\#\/howto(\/.*)?/,function(){
		if (hasToReturn(this,"howto")){return;}
		this.app.swap('');
		this.partial('content/howto.template');
		changeSection("howto");
	});

	this.get("#/user",function(){
		if (hasToReturn(this,"user")){return;}
		if (!SessionManager.getUser()){
			this.redirect("#/");
			var context = this;
			setTimeout(function(){
				if(SessionManager.getUser()){
					context.redirect("#/user");
					context.app.refresh();
				}
			},500);
			return;
		}
		this.app.swap('');
		this.partial('content/user.template',{
			user: SessionManager.getUser()
		});
		clearLinks();
	});

	this.post("#/user",function(){
		// Function defined in gui/user.js file.
		var context = this;
		updateUser(this.params['username'],this.params['password'],this.params['old-password'])
		.then(function(result){
			if(result){
				context.redirect("#/user");
				context.app.refresh();
			}
		});
	});
        
  this.get("#/login", function() {
    // if already on "#/login" don't reload
    if (hasToReturn(this,"login")){return;}
    // clean container
    this.app.swap('');
    // fill container with html file login.template
    this.partial('content/login.template');
    clearLinks();
  });
        
  this.post("#/login", function() {
    //Function defined in the login.js file, which should be loaded. 
    logInFormSubmitted(this.params['userName'],this.params['password'], this);
    // return false to prevent form from being submitted (default)
    return false; 
  });

  this.get("#/logout", function() {
  	SessionManager.logOut();
  	this.redirect("#/");
  });

  this.get("#/createUser", function() {
  	if (hasToReturn(this,"createUser")){return;}
		this.app.swap('');
		this.partial('content/createUser.template');
		clearLinks();
  });

  this.post("#/createUser", function() {
  	this.app.swap('');
  	//Function defined in createUser.js
  	var context = this;
  	var username = this.params['username'];
  	createUser(username,this.params['password'])
  		.then(function(result){
  			window.location.hash="";
  			hasToReturn(context,"welcome");
  			context.partial('content/welcome.template', {
  				user: username,
  				error: !result
  			});
  		})
  	clearLinks();
  	return false;
  })

  this.get("/",function(){});

});

// When the page has finished loading start the app
// with the default route, which is the root.
$(function(){
	SammyGlobalApp.run("#/")
	SessionManager.init(SammyGlobalApp);
});