/* Module that managed the session state of the page
 * It changes the default ajax parameters to include
 * the token. This way all request are automatically
 * authenticated. It also stores the current user in
 * case any module needs it for diaplying anything.
 */
var SessionManager = function () {


	//private variables:
	var sessionStorage = null;
	var currentUser = null;

	
	//private methods:
	
	// Initializes the session storage if it hasn't been
	// already initialized.
	var initSessionStorage = function(sammyApp){
		if (sessionStorage){ return; }
		sessionStorage = sammyApp.store('session');
	}

	// Set an alert in all ajax calls when the response
	// is an 403 status a.k.a Forbidden
	var setNoLoginAlert = function(){
		$.ajaxSetup({
			statusCode: {
				403: function(){
					if (currentUser == null){
						bootbox.alert("This action requires log-in");
					}else{
						bootbox.alert("Action not allowed")
					}
				}
			}
		});
	} 

	// Checks if there is a session recorded in the session
	// storage of the browser. Should be called at page startup
	// to allow continuing a session after page refresh.
	// Returns deferred true or false.
	var checkLoginInStorage = function(){
		// If there is a token stored in the browser
		if (sessionStorage.exists('token')){

			// Create a deferred object that will get the result when
			// the request is over
			var deferredResult = new $.Deferred();
			changeLoginState("loading");

			// Ajax call that tries to obtain the data about the session
			// to check if the token is still valid
			Sessions.getSession(sessionStorage.get("token"),{
				statusCode : {}, //Disable no login alerts
				headers: {"x-session-token": sessionStorage.get('token')},
				success: function(data){
					deferredResult.resolve(true);
					changeLoginState("loggedIn",data);
				},
				error: function(){
					deferredResult.resolve(false);
					changeLoginState("loggedOut");
				}
			});

			return $.when(deferredResult);
		}else{
			return new $.Deferred().resolve(false);
		}
	}

	// From the point this function is called, all
	// calls to the api will have the x-session-token
	// set with the right value without any need to specify.
	var enableAjaxHeaders = function(token){
		$.ajaxSetup({
			headers: {"x-session-token": token}
		});
	}

	// Restores the changes made by the function above.
	var disableAjaxHeaders = function(){
		$.ajaxSetup({
			headers: {}
		});
	}

	// This function keeps a list of the actions needed to change
	// to one of the three possible application steps. Data is an
	// optional parameter that is only used in the case of the login
	// and should be an object with username and token set.
	var changeLoginState = function(newState,data){
		switch(newState){
			case "loading":
				// Display loading in the interface.
				setLoginLoadingString()
				break;

			case "loggedOut":
				// Forget the token
				sessionStorage.clear("token");
				// Delete the user data stored
				currentUser = null;
				// Display a new login button
				setLoggedOutString();
				// Reset the headers
				disableAjaxHeaders();
				break;

			case "loggedIn":
				// Store the new token to make it persist across refreshs
				sessionStorage.set("token",data.token);
				// And the user, for future reference
				currentUser = data.user;
				// Display the authenticated user
				setLoggedInString(currentUser);
				// Authenticate all future requests to the api
				enableAjaxHeaders(data.token);
				break;
		}
	}

	// Public functions
	return  {
		init: function(sammyApp){
			setNoLoginAlert();
			initSessionStorage(sammyApp);
			checkLoginInStorage();
		},
		logIn: function(username, password){
			var result = new $.Deferred();
			Sessions.createSession(username,password,{
				statusCode: {}, //Disbale forbidden alerts
				success: function(data){
					result.resolve({result: true, message: "Welcome "+data.user.username});
					changeLoginState("loggedIn",data);
				},
				error: function(){
					result.resolve({result: false, message: "Incorrect username or password"});
					changeLoginState("loggedOut");
				}
			});
			return $.when(result);
		},
		logOut: function(){
			// Header has already been set so there is no need for it.
			Sessions.deleteSession(sessionStorage.get("token"));
			changeLoginState("loggedOut");
		},
		isLoggedIn: function(){
			return (currentUser != null);
		},
		getUser: function(){
			return currentUser;
		},
		setUser: function(newUser){
			currentUser = newUser;
			setLoggedInString(newUser);
		}
	};

}();