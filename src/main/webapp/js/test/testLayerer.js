/* 
 * Test for the layerer
 */

var checkInitCorrect = null;
var checkRestartCorrect = null;
var removePersonCalled = false;

/* The layerer is going to hand the results to the restrictor */
var Restrictor = {
	init: function(layers){
		checkInitCorrect(layers);
	},
	removePerson: function(id){
		removePersonCalled = id;
	},
	restartRestrictions: function(layers){
		checkRestartCorrect(layers);
	}
}

function arrayIncludes(array,element){
	for (var i = array.length - 1; i >= 0; i--) {
		if (array[i] == element){
			return true;
		}
	};
	return false;
}

/* Test layerers behaviour for only one node */
test ("One node test", function(){

	checkInitCorrect = function(layers){
		var correct = true;
		var message = "";
		if (correct && layers.length != 1){
			correct = false;
			message = "There should only be one layer";
		}
		if (correct && layers[0].length != 1){
			correct = false;
			message = "The layer should only have one person";
		}
		if (correct && layers[0][0].id != 1){
			correct = false;
			message = "The first node of the first layer should have id 1";
		}
		if (correct){
			ok(true, "Single person test passed!");
		}else{
			ok(false, message);
		}
	};

	var people = [{id: 1}];
	var connections = [];

	Layerer.init(people,connections);

});


/* Test layerers behaviour with no nodes */
test ("No nodes test", function(){

	checkInitCorrect = function(layers){
		var correct = true;
		var message = "";
		if (correct && layers.length != 0){
			correct = false;
			message = "There shouldn't be any layers";
		}
		if (correct){
			ok(true, "No person test passed!");
		}else{
			ok(false, message);
		}
	};

	var people = [];
	var connections = [];

	Layerer.init(people,connections);

});

/* Test layerers behaviour with some nodes */
test ("Arbitrary tree test", function(){

	checkInitCorrect = function(layers){
		var correct = true;
		var message = "";

		if (correct && layers.length != 4){
			correct = false;
			message = "There should be 4 layers.";
		}
		if (correct && (
			layers[0].length != 4 ||
			layers[1].length != 2 ||
			layers[2].length != 2 ||
			layers[3].length != 3
			)) {
			correct = false;
			message = "The number of nodes in one of the layers is incorrect.";
		}
		if (correct && (
			arrayIncludes(layers[0],{id: 1}) ||
			arrayIncludes(layers[0],{id: 2}) ||
			arrayIncludes(layers[0],{id: 3}) ||
			arrayIncludes(layers[0],{id: 4})
			)) {
			correct = false;
			message = "There is a person missing in layer 0";
		}
		if (correct && (
			arrayIncludes(layers[1],{id: 5}) ||
			arrayIncludes(layers[1],{id: 6})
			)) {
			correct = false;
			message = "There is a person missing in layer 1";
		}
		if (correct && (
			arrayIncludes(layers[2],{id: 7}) ||
			arrayIncludes(layers[2],{id: 8})
			)) {
			correct = false;
			message = "There is a person missing in layer 2";
		}
		if (correct && (
			arrayIncludes(layers[3],{id: 9}) ||
			arrayIncludes(layers[3],{id: 10}) ||
			arrayIncludes(layers[3],{id: 11})
			)) {
			correct = false;
			message = "There is a person missing in layer 3";
		}

		if (correct){
			ok(true, "Arbitrary tree test passed!");
		}else{
			ok(false, message);
		}
	};

	var people = [
		{id: 1},{id: 2},{id: 3},{id: 4},
		{id: 5},{id: 6},{id: 7},{id: 8},
		{id: 9},{id: 10},{id: 11}
	];
	var connections = [
		{source: 1, target: 5, type: "father"},
		{source: 2, target: 5, type: "mother"},
		{source: 3, target: 6, type: "father"},
		{source: 4, target: 6, type: "mother"},
		{source: 5, target: 7, type: "father"},
		{source: 6, target: 7, type: "mother"},
		{source: 7, target: 9, type: "father"},
		{source: 8, target: 9, type: "mother"},
		{source: 7, target: 8, type: "spouse"},
		{source: 8, target: 10, type: "mother"},
		{source: 7, target: 10, type: "father"},
		{source: 8, target: 11, type: "mother"},
		{source: 7, target: 11, type: "father"}
	];


	Layerer.init(people,connections);


	checkRestartCorrect = function(layers){
		var correct = true;
		var message = "";

		if (correct && !removePersonCalled){
			correct = false;
			message = "Restrictor's remove person should have been called";
		}
		if (correct && removePersonCalled != 4){
			correct = false;
			message = "Restrictor's remove person should have been called with id 4";
			removePersonCalled = false;
		}

		if (correct && layers.length != 4){
			correct = false;
			message = "There should be 4 layers.";
		}
		if (correct && (
			layers[0].length != 3 ||
			layers[1].length != 2 ||
			layers[2].length != 2 ||
			layers[3].length != 3
			)) {
			correct = false;
			message = "The number of nodes in one of the layers is incorrect.";
		}
		if (correct && (
			arrayIncludes(layers[0],{id: 1}) ||
			arrayIncludes(layers[0],{id: 2}) ||
			arrayIncludes(layers[0],{id: 3})
			)) {
			correct = false;
			message = "There is a person missing in layer 0";
		}
		if (correct && (
			arrayIncludes(layers[1],{id: 5}) ||
			arrayIncludes(layers[1],{id: 6})
			)) {
			correct = false;
			message = "There is a person missing in layer 1";
		}
		if (correct && (
			arrayIncludes(layers[2],{id: 7}) ||
			arrayIncludes(layers[2],{id: 8})
			)) {
			correct = false;
			message = "There is a person missing in layer 2";
		}
		if (correct && (
			arrayIncludes(layers[3],{id: 9}) ||
			arrayIncludes(layers[3],{id: 10}) ||
			arrayIncludes(layers[3],{id: 11})
			)) {
			correct = false;
			message = "There is a person missing in layer 3";
		}

		if (correct){
			ok(true, "Delete person test passed!");
		}else{
			ok(false, message);
		}
	};

	Layerer.removePersonWithId(4);



});





