/**
 *  See TreeResource.java, ConnectionsResource.java and 
 *      http://docs.familytree.apiary.io/
 *   NOTE: All methods return deferred object
 *   
 */

/** 
 * All methods have an optional options parameter that can contain
 * any specific jquery ajax options to override the method ones.
 */
var Tree = function() {

    var baseUri = "api/tree";
    
    var baseOptions = {
        dataType: "json",
        url: baseUri
    };
    
    // Public API with AJAX calls to RESTful services.
    return {
        // TreeResource
        getConnections: function(id, options) {
            methodOptions = {
                url: baseUri + "/connections/" + id
            };
            options = $.extend({},baseOptions,methodOptions,options);
            return $.ajax(options);
        },
        
        
        getTreeRoot: function(options) {
            methodOptions = {
                url: baseUri + "/root"
            };
            options = $.extend({},baseOptions,methodOptions,options);
            return $.ajax(options);
        },
        
        // ConnectionsResource
        createConnection: function(srcId, dstId, relationType, options) {
            methodOptions = {
                url: baseUri + "/connections/" + srcId + "/" + dstId,
                type: "POST",
                data: {
                    type: relationType
                }
            };
            options = $.extend({}, baseOptions, methodOptions, options);
            return $.ajax(options);
        },
        
        getConnectionsForPerson: function(id, options) {
            methodOptions = {
                url: baseUri + "/connections/" + id
            };
            options = $.extend({}, baseOptions, methodOptions, options);
            return $.ajax(options);
        },
        
        deleteConnection: function(srcId, dstId, options) {
            methodOptions = {
                url: baseUri + "/connections/" + srcId + "/" + dstId,
                type: "DELETE"
            };
            options = $.extend({}, baseOptions, methodOptions, options);
            return $.ajax(options);
        }
    };
}();
