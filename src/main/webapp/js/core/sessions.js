/**
 *  See SessionsResource.java and http://docs.familytree.apiary.io/
 *   NOTE: All methods return deferred object  
 */

/** 
 * All methods have an optional options parameter that can contain
 * any specific jquery ajax options to override the method ones.
 */
var Sessions = function(){

	var baseUri = "api/sessions/";

	var baseOptions = {
		dataType: "json",
		url: baseUri
	};

	return {

		// Not necessary to call, but can be used to replace the baseUri.
		init: function(uri){
			// Only replace if there is a value in uri
			baseUri = uri || baseUri;

			baseOptions.url = baseUri;
		},

		// Get the current session details (or check if you are logged in)
		getSession: function(token,options){
			methodOptions = {
				url: baseUri + token
			};
			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		},

		// Create a new session (a.k.a. log in)
		createSession: function(username,password,options){
			methodOptions = {
				type: "POST",
				data: {
					username: username,
					password: password
				}
			};
			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		},

		// Delete a session (a.k.a log)
		deleteSession: function(token,options){
			methodOptions = {
				url: baseUri + token,
				type: "DELETE"
			};
			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		}
	}

}();