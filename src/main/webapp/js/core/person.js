/**
 *  See PeopleResource.java and http://docs.familytree.apiary.io/
 *   NOTE: All methods return deferred object  
 */

/** 
 * All methods have an optional options parameter that can contain
 * any specific jquery ajax options to override the method ones.
 */
var Person = function() {

    var baseUri = "api/people";
    
    var baseOptions = {
        dataType: "json",
        url: baseUri
    };

    var dateToString = function(date){
        return date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear();
    }

    // Convert the information returned by the api to the one we want to have in the app.
    var filterPerson = function(person){
        var result = {
            name: person.name,
            surname: person.surname,
            gender: person.gender,
            id: person.id,
        };
        result.birthdate = dateToString(new Date(person.birthdate));
        if (person.deathdate){
            result.deathdate = dateToString(new Date(person.deathdate));
        }
        return result;
    }
    
    // Public API with AJAX calls to RESTful services.
    return {
        getById: function(id, options) {
            methodOptions = {
                url: baseUri + "/" + id
            };
            options = $.extend({},baseOptions,methodOptions,options);
            return $.ajax(options).then(filterPerson);
        },
        
        getByName: function(name, options) {
            methodOptions = {
                url: baseUri + "/" + name
            };
            options = $.extend({},baseOptions,methodOptions,options);
            return $.ajax(options).then(function(people){
                res = [];
                for (var i = people.length - 1; i >= 0; i--) {
                    res.push(filterPerson(people[i]));
                };
                return res;
            });
        },
        
        createPerson: function(firstName, lastName, gender,
                birthDate, deathDate, options) {
            methodOptions = {
                type: "POST",
                data: {
                    firstName: firstName,
                    lastName: lastName,
                    gender: gender,
                    birthDate: birthDate
                }
            };
            if (deathDate){
                methodOptions.data.deathDate = deathDate;
            }
            options = $.extend({}, baseOptions, methodOptions, options);
            return $.ajax(options);
        },
        
        updatePerson: function(id, firstName, lastName, gender, 
                birthDate, deathDate, options) {
            methodOptions = {
                url: baseUri + "/" + id,
                type: "PUT",
                data: {
                    id: id,
                    firstName: firstName,
                    lastName: lastName,
                    gender: gender,
                    birthDate: birthDate,
                    deathDate: deathDate
                }
            };
            options = $.extend({}, baseOptions, methodOptions, options);
            return $.ajax(options);
        },
        
        deletePerson: function(id, options) {
            methodOptions = {
                url: baseUri + "/" + id,
                type: "DELETE"
            };
            options = $.extend({}, baseOptions, methodOptions, options);
            return $.ajax(options);
        }
    };
}();

