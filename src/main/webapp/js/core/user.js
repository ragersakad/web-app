/**
 *  See UsersResource.java and http://docs.familytree.apiary.io/
 *   NOTE: All methods return deferred object  
 */

/** 
 * All methods have an optional options parameter that can contain
 * any specific jquery ajax options to override the method ones.
 */
var User = function(){

	var baseUri = "api/users/";

	var baseOptions = {
		dataType: "json",
		url: baseUri
	};

	return {

		getByUsername: function(username,options){
			methodOptions = {
				url: baseUri + username
			};
			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		},

		create: function(username,password,options){
			methodOptions = {
				type: "POST",
				data: {
					username: username,
					password: password
				}
			};

			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		},

		update: function(username,password,oldPassword,options){
			var data = {};

			if(username){
				data["new-username"] = username;
			}

			if(password){
				data["new-password"] = password;
				data["old-password"] = oldPassword;
			}

			methodOptions = {
				url: baseUri + SessionManager.getUser().username,
				type: "PUT",
				data: data
			}

			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		},

		remove: function(username,options){
			methodOptions = {
				url: baseUri + username,
				method: "DELETE"
			};
			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		},

		checkUsernameAvailable: function(username,options){
			methodOptions = {
				url: baseUri + "available/" + username
			};
			options = $.extend({},baseOptions,methodOptions,options);
			return $.ajax(options);
		}
	};

}();