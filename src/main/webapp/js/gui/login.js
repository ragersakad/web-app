// Function called when the login form has been submitted.
function logInFormSubmitted( username, password, sammyContext ){
	SessionManager.logIn(username, password, {status: {}}).then(function(answer){
		if(answer.result){
			sammyContext.redirect("#/");
		}else{
			$("#login-form").addClass("has-error");
		}
	});
}

