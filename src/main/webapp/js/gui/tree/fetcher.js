/* This module is in charge of communication the users actions to the database
 * and of fetching the data requested by the user. When asked to fetch the tree for 
 * a user, it fetches that person information and connections and the information of
 * all the peple he is related to. */

var Fetcher = function() {

    var listOfConnections; // Lsit of all connections being shown in the tree
    var listOfPeople; // List of all people being shown on the tree.

    // When asked to push a connection for a new person to the database 
    // this function will help setting the correct id in the connection
    // when the new person has already been pushed and its id is known.
    var replaceIdInConnection = function(connection, oldId, newId){
        if (connection.source === oldId){
            connection.source = newId;
        }
        if (connection.target === oldId){
            connection.target = newId;
        }
        return connection;
    };

    /**
     * Fetches all connections for person with given id. 
     * Fetches person with given id and connected persons
     * and the connections between them.
     *
     * This function could be greatly sped up with some caching.
     * 
     * @param {type} id Id of person.
     * @returns {undefined}
     */
    var fetchTreeForPerson = function(id, doneCallback) {
        var numberOfFetchedPeople = 1;
        listOfPeople = [];
        listOfConnections = [];

        /*** fetch first person ***/
        Person.getById(id).then(function(person) {
            listOfPeople.push(person);
            numberOfFetchedPeople -= 1;
        }).then(function() {

            /*** Then get his connections ***/
            Tree.getConnectionsForPerson(id).then(function(connections) {

                // We keep the number of people being fetched to correctly identify
                // when everyone has been fetched (lightswitch algorithm).
                numberOfFetchedPeople += connections.length;
                listOfConnections = connections;
                
                /*** A dictionary (for easy search) with the people being fetched ***/
                var peopleBeingFetched = {};
                for (var i = 0; i < listOfConnections.length; i++) {
                    var c = listOfConnections[i];
                    var oId = (c.source === id) ? c.target : c.source;
                    peopleBeingFetched[oId] = true;
                };
                
                /*** Then get the data for all the people related to the first person ***/
                for (var otherId in peopleBeingFetched) {

                    Person.getById(otherId).then(function(otherId) {
                        return function (otherPerson){
                            listOfPeople.push(otherPerson);
                            
                            /** Lastly get the connections for the person that has been just fetched
                             ** and keep the ones relating him to other people in the tree **/
                            Tree.getConnectionsForPerson(otherId).then(function(otherConnections){
                                for (var j = 0; j < otherConnections.length; j++) {
                                    var oc = otherConnections[j];
                                    var otherOtherId = (oc.source == otherId) ? oc.target : oc.source;
                                    // If the connection involves other people in the tree which are not the
                                    // root, add the connection to the list being later passed to the layerer
                                    if (peopleBeingFetched[otherOtherId] == true && otherOtherId != id){
                                        listOfConnections.push(oc);
                                    }
                                };
                                
                                // Lightswitch algorithm. If every person has been fetched
                                // do the callback.
                                numberOfFetchedPeople -= 1;    
                                if (numberOfFetchedPeople === 0) {
                                    doneCallback();
                                }
                            });
                        }
                    }(otherId)); // This wrapper is done to prevent javascript's closure
                                 // from providing the wrong id.
                }

                if (listOfConnections.length == 0){
                    doneCallback();
                }
            });
        });
    };

    // Decides which is the node taht is going to be shown to the user.
    // It will return "empty" if the tree should start empty.
    // It returns a deferred obejct.
    var decideRootNode = function(){
        var deferred = new $.Deferred();
        Tree.getTreeRoot().then(function(data){
            deferred.resolve(data.value);
        }, function(){
            deferred.resolve("empty");
        });
        return $.when(deferred);
    }

    return {
        //initiates the root
        init: function() {
            
            listOfPeople = [];
            listOfConnections = [];

            decideRootNode().then(function(result){
                if (result == "empty"){
                    TreeController.startEmptyMode();
                }else{
                    fetchTreeForPerson(result, function(){
                        Layerer.init(listOfPeople, listOfConnections);
                    });
                }
            });
        },

        // Called when manually inserting a new person or connection on the tree.
        // It pushes the changes to the api and then updates the representation.
        // If the changes are not accepted by the api the user doesn't see them 
        // in his interface.
        add: function(person,connection){
            // If a person is added then we have to wait until we get back its id to push the connection.
            if (person){
                Person.createPerson(person.name, person.surname, person.gender, person.birthdate, person.deathdate).then(function(result) {
                    if (!connection){
                        person.id = result;
                        Layerer.init([person],[]); // This comes from empty mode in which init hasn't been called yet.
                        return;
                    }

                    // Replace the id for a new one in the connection.
                    connection = replaceIdInConnection(connection, person.id, result); 
                    person.id = result;
                    Tree.createConnection(connection.source, connection.target, connection["type"]).then(function(result) {
                        Layerer.add([person], [connection]);
                    });
                });
            // If only a connection needs to be stored.
            }else if(connection){
                Tree.createConnection(connection.source, connection.target, connection["type"]).then(function(result) {
                    Layerer.add([], [connection]);
                });
            }
        },

        // Cahnge the root of the tree to a new person.
        moveTo: function(id){
            Layouter.collapseToId(id).then(function(){
                listOfPeople = []; // Shorter than new Array()
                listOfConnections = [];
            
                fetchTreeForPerson(id, function(){
                    Layerer.reset(listOfPeople, listOfConnections);
                });
            });
        },
        
        //removes person
        removePerson: function(id) {
            Person.deletePerson(id).then(function() {
                Layerer.removePersonWithId(id);
            });
        }
    };

}();