/* This file manages the user actions over the tree and
 * initialises the whole tree render */


var TreeController = function(){

	// -1 if no node is being connected, -2 if it is a new node that
	// hasn't been stored in the database yet and a normal id for other cases
	var nodeBeingConnected = -1;

	// Fetch the add-person dialog html from the template
	// and store it to create the popover in the add button.
	var addPersonHtml = $("#addPersonDialog").html();
	$("#addPersonDialog").empty();

	// Wether a person has been created and not conencted yet.
	// In case it has been created it holds its data.
	var personBeingCreated = null;

	// Stores the source and the target of the nodes that will be connected
	// from the time they are selected to the time the relation is built.
	var relationBeingBuilt = {};

	// Variable to avoid showing the pop-up explaining how to
	// connect nodes more than once.
	var hasPopUpBeenShown = false;

	// Wether the tree displayed is empty or not. If it is, new persons should
	// be added inmediatly instead of waiting for a connection to be made
	var empty = false;

	// Return the person information given its id.
	var nodeForId = function(id){
		// The newly created person has temporary reserved id -2
		if (id == -2) {
			return personBeingCreated;
		}
		// If it is not the new person, get the information from the layerer,
		// which keeps a list of all people.
		var allPeople = Layerer.getAllPeople();
		for (var i = allPeople.length - 1; i >= 0; i--) {
			if (allPeople[i].id == id){
				return allPeople[i];
			}
		};
	}

	// Get all connections involving a certain id from the layerer.
	var connectionsForId = function(id){
		var all = Layerer.getAllEdges();
		var res = [];
		for (var i = 0; i < all.length; i++) {
			var edge = all[i];
			if (edge.source == id || edge.target == id){
				res.push(edge);
			}
		};
		return res;
	}

	// Returns information about the relations of that user
	var analyseRelations = function(relations,id){
		var result = {
			parents: [], // The parents of that person
			spouse: 0, // The id of his/her spouse
			children: [], // The ids of his/her children
			related: [] // The ids of all related people
		}
		for (var i = 0; i < relations.length; i++) {
			var relation = relations[i];
			if (relation["type"] == "father" || relation["type"] == "mother"){
				if (relation.source == id) {
					result.children.push(relation.target);
					result.related.push(relation.target);
				}else{
					result.parents.push(relation.source);
					result.related.push(relation.source);
				}
			}else if(relation["type"] == "spouse"){
				if (relation.source == id) {
					result.spouse = relation.target
					result.related.push(relation.target);
				}else{
					result.spouse = relation.source;
					result.related.push(relation.source);
				}
			}
		};
		return result;
	}

	// Given two ids check and the information about their relations
	// check in which ways they can be connected. This method doesn't
	// take age into account, YET.
	var enablePossibleConnections = function(id1,id2){
		// Diable all possible connections
		$("#fatherRelationOption, #motherRelationOption, #spouseRelationOption, #childRelationOption")
			.attr("disabled","disabled");

		var id1Rel = analyseRelations(connectionsForId(id1),id1);
		var id2Rel = analyseRelations(connectionsForId(id2),id2);

		var id1Node = nodeForId(id1);
		var id2Node = nodeForId(id2);

		// Test if already related
		for (var i = 0; i < id1Rel.related.length; i++) {
			if (id1Rel.related[i] == id2){
				bootbox.alert("The nodes selected are already related");
				return false;
			}
		};

		var numberOfPossibleRelations = 0;

		// Test if father/mother relationship
		if (id2Rel.parents.length < 2){
			var option = (id1Node.gender == "male") ? "father" : "mother" ;
			$("#"+option+"RelationOption").removeAttr("disabled");
			numberOfPossibleRelations += 1;
		}
		
		// Test if spouse relationship
		if (!id1Rel.spouse && !id2Rel.spouse){
			$("#spouseRelationOption").removeAttr("disabled");
			numberOfPossibleRelations += 1;
		}
		
		// Test if child relationship
		if (id1Rel.parents.length < 2){
			$("#childRelationOption").removeAttr("disabled");
			numberOfPossibleRelations += 1;
		}
		
		if(numberOfPossibleRelations = 0){
			bootbox.alert("The nodes selected can't be related in any way.");
			return false;
		}

		$("#sourceName").text(id1Node.name + " " + id1Node.surname);
		$("#targetName").text(id2Node.name + " " + id2Node.surname);

		return true;
	};

	// Display the connection picker.
	var displayPossibleConnections = function(id1,id2){
		
		if (enablePossibleConnections(id1,id2)){
			relationBeingBuilt = {
				source: id1,
				target: id2
			};
			$('#relationModal').modal('show');
		}
	};

	// Method taht gets called when the connection dialog has been submitted.
	// It colletcs the relation information and tells the fetcher that a person
	// and a connection have been added.
	var relationModalSubmitted = function(){
		$('#relationModal').modal('hide');
		var type = $("#relationSelect").val();
		var connection;
		var person = null;
		if (type == "child"){
			var option = (nodeForId(relationBeingBuilt.target).gender == "male") ? "father" : "mother";
			connection = createConnection(relationBeingBuilt.target,relationBeingBuilt.source,option);
		}else{
			connection = createConnection(relationBeingBuilt.source,relationBeingBuilt.target,type);
		}
		if (relationBeingBuilt.target == -2){ // If new node it is always the target
			person = personBeingCreated;
			changeToPlus();
		}
		Fetcher.add(person,connection);
	}

	// Build a connection object with the parameters specified.
	var createConnection = function(id1,id2,type){
		return {
			source: id1,
			target: id2,
			type: type
		};
	};

	// Move the tree to the id specified.
	var goToNode = function(id){
		Fetcher.moveTo(id);
	};

	// Sets up the plus button, basically its popover 
	// and a handler to be able to set up the form handlers.
	var setUpPlusButton = function(){
		$("#plus-button").popover({
			html: true,
			placement: 'top',
			title: 'Add person',
			content: addPersonHtml,
			container: "#tree-graph"
		});
		$("#plus-button").bind("click",function(){
			createdNewPersonUI();
		});
	};

	// The new person created by the user has been clicked.
	var newNodeClicked = function(){
		if (nodeBeingConnected == -1){
			return;
		}
		self.nodeClicked(-2);
	}

	// Set up the handler of a new node's person
	var setUpNewNode = function(){
		var node = $("#new-node");
		node.bind("click",newNodeClicked);
	}

	// Bind the create connection button to its action.
	var setUpModal = function(){
		$("#createConnectionButton").bind("click",relationModalSubmitted);
	}

	// Change the bottom of the interface from a node to a plus button.
	var changeToPlus = function(){
		$("#plus-button").css("display","block");
		$("#new-node").css("display","none");
		setUpPlusButton();
		personBeingCreated = null;
	};

	// Change the bottom of the interface to show a node instead of a plus button.
	// Also set all the information of the node.
	var changeToNewNode = function(data){
		$("#plus-button").popover('destroy');
		$("#tree-graph .popover").remove();
		$("#plus-button").css("display","none");
		$("#new-node").css("display","block");
		$("#new-node > h1").text(data.name + " " + data.surname);
		$("#new-node > h2").text(data.birthdate + " " + (data.deathdate? data.deathdate : "&#8734;&nbsp;&nbsp;&nbsp;"));
		personBeingCreated = data;
	}

	// Show the user an alert indicating how to finish creating a connection.
	// Only showed once.
	var showHintPopUp = function(){
		bootbox.alert("Click on any other node to create a relation");
	}

	// Store which node is being connected. -1 for no node.
	// Also displays a light glow around the node to indicate
	// that it is the one being used for connection.
	var setNodeBeingConnected = function(id){
		if (!hasPopUpBeenShown){
			showHintPopUp();
			hasPopUpBeenShown = true;
		}
		if (id == -1){
			$(".glow").removeClass("glow");
		}else{
			Layouter.getNode(id).data.jqueryNode.addClass("glow");
		}

		nodeBeingConnected = id;
	}

	// The public methods of the controller.
	var self = {
		init: function(){
			setUpPlusButton();
			setUpNewNode();
			setUpModal();
			Fetcher.init();
		},
		// Handler of the connect buttons on the nodes.
		connectClicked: function(id){
			if (nodeBeingConnected != -1 && nodeBeingConnected != id) {
				displayPossibleConnections(nodeBeingConnected,id);
				setNodeBeingConnected(-1);
			}else if(nodeBeingConnected == id){
				setNodeBeingConnected(-1);
			}else{
				setNodeBeingConnected(id);
			}
		},
                deleteClicked: function(id){
			Fetcher.removePerson(id);
			bootbox.alert("We are sorry but at the moment you have to click on a non-deleted node to see your changes.");
		},
		// Handler for clicks anywhere in a node except for the connect and delete buttons.
		nodeClicked: function(id){
			if (nodeBeingConnected == -1) {
				return goToNode(id);
			}
			if (nodeBeingConnected == id){
				return setNodeBeingConnected(-1);
			}
			displayPossibleConnections(nodeBeingConnected,id);
			setNodeBeingConnected(-1);
		},
		// Handler for the create person button on the create-person form.
		createPersonFormSubmitted: function(data){
			if(empty){
				empty = false;
				$("#plus-button").popover('hide');
				Fetcher.add(data);
				return;
			}
			data.id = -2;
			changeToNewNode(data);
		},
		// Start the tree with no people being displayed.
		startEmptyMode: function(){
			empty = true;
		}

	};

	return self;

}();