/**
 * Takes a family tree structure (a set of persons and connections) and
 * transforms them into a layered tree representation of type
 *      tree: [layer0:[person0, person1...] layer1:[person0, person1..] ...]
 *      edges: [relation0, relation1 ...]
 *      
 *      Object person must have attribute "id". Any other attributes aren't processed.
 *      Object relation must have attributes "source", "target" and "type".
 */
var Layerer = function() {

    var allPeople;
    var allEdges;

    // contains the remaining persons to add to tree
    var remainingPeople;
    // contains relations
    var edges;
    
    // a layered representation of the family-tree
    // tree: [layer0:[person0, person1...] layer1:[person0, person1..] ...]
    var tree;
    // A layer to which people without relations are added.
    var defaultLayer;
    
    // private functions
    var doLayering = function() {
        while (remainingPeople.length > 0) {
            recursiveLayerer
            (null, remainingPeople[0], null);
            //alertTree();
        }
    };
    
    var removeFromLayering = function(ps) {
        var toRemove = ps;
        for (var person in toRemove) {
            var p = toRemove[person];   // current person to remove
            
            //alert(p.id);
            removeFromTree(p.id);
            // remove all p's relations from edges
            var prels = getRelationsFor(p);
            for (var i in prels) {
                var r = prels[i];       // current relation to remove
                var index = findWithAttr(edges, 'id', r.id);
                if (index > -1) 
                    edges.splice(index, 1);
            }
        }
    };
    
    // helper: return index of object in array with 'attribute': value
    var findWithAttr = function(array, attr, value) {
        for (var i=0; i < array.length; i++) {
            if (array[i][attr] === value)
                return i;
        }
        return -1;
    };
    
    // get all relations where person is either source or target
    var getRelationsFor = function(person) {
        var relations = new Array();
        for (var i in edges) {
            var r = edges[i];
            if (r.source === person.id || r.target === person.id) {
                relations.push(r);
                //alert("getRelationsFor: " + i + ": " + r.source + ": " + r.target);
            }
        }
        return relations;
    };
    
    // for debugging
    var alertTree = function() {
        var stringTree = "Tree is:\n";
        for (var i = 0; i < tree.length; i++) {
            stringTree += "\tlayer " + i + ": [ ";
            for (var j = 0; j < tree[i].length; j++) {
                stringTree += "person " + j + " id: " + tree[i][j].id + ", ";
            }
            stringTree += " ]\n";
        }
        alert(stringTree);
    };
    
    /**
     * To start a recursion, call with (null, p, null). Layering will be done
     * until all of person p's relatives and their relatives are added.
     * It does not matter which person p the recursion starts with.
     * 
     * NOTE: recursion must be started for each island of related people. 
     * 
     * @param {type} prevp  The person added before person p.
     * @param {type} p      The person to add to the tree.
     * @param {type} rel    The relation r: {source = prevp, target = p, type = type}
     * @returns {unresolved}
     */
    var recursiveLayerer = function(prevp, p, rel) {
        addToTree(prevp, p, rel);
        // remove p from remainingPeople
        var index = findWithAttr(remainingPeople, 'id', p.id);
        if (index > -1) {
            remainingPeople.splice(index, 1);
            // alert("Recursive layerer remainingPeople.length: " + remainingPeople.length);
        }
        
        var psRelations = getRelationsFor(p);
        if (psRelations.length === 0) {
//            alert("Recursive layerer return, no more relations for p");
            return;
        }
        
        for (var i in psRelations) {
            var relation = psRelations[i];
            var otherId = (relation.source === p.id)?relation.target:relation.source;
            var otherPersonIndex = findWithAttr(remainingPeople, 'id', otherId);
            // other person already in tree, return
            if (otherPersonIndex === -1) {
                // alert("Recursive layerer continue, otherPerson already in tree");
                continue;
            }
//            alert("Recursive layerer loop index " + i + 
//                    "\n\trelation.source id: " + relation.source +
//                    "\n\trelation.target id: " + relation.target +
//                    "\n\trelation.type: " + relation.type);
            recursiveLayerer(p, remainingPeople[otherPersonIndex], relation);
        }
    };
    
    var addToTree = function(pInTree, pToAdd, relation) {
        var debugString = "addToTree: ";
        // if there is no tree, make one
        if (tree.length === 0) {
            defaultLayer.push(pToAdd);
            tree.push(defaultLayer);
            return;
        }
        if (defaultLayer.length === 0) {    // can happen because of removeFromTree(p)
            defaultLayer = tree[0];
        }
        if (pInTree === null) {
            defaultLayer.push(pToAdd);
            tree.push(defaultLayer);
            return;
        }


        // look for pInTree
        // for each layer in tree
        for (var i = 0; i < tree.length; i++) {
            // for each person in current layer
            var currentLayer = tree[i];
            for (var j = 0; j < currentLayer.length; j++) {
                // match :)
                var currentPerson = currentLayer[j];
                if (currentPerson.id === pInTree.id) {
//                    alert(debugString += pToAdd.id + 
//                        "\n\trelative to: " + currentPerson.id +
//                        "\n\trelationType: " + relation.type);
                    switch (relation.type) {
                        case "father":
                            // same case as next
                        case "mother":
                            // pInTree is parent, add pToAdd to a layer below
                            if (relation.source === pInTree.id) {
                                if (i + 1 >= tree.length) {
                                    var newBottomLayer = new Array();
                                    newBottomLayer.push(pToAdd);
                                    tree.push(newBottomLayer);  // add to new bottom layer
                                    
                                } else {
                                    //alert("add to existing layer");
                                    tree[i + 1].push(pToAdd);   // add to existing lower layer
                                }
                            }
                            // pToAdd is parent, add pToAdd to a layer above
                            else {
                                if (i - 1 < 0)
                                    tree.unshift([pToAdd]);     // add to new top layer
                                else
                                    tree[i - 1].push(pToAdd);   // add to existing layer above current
                            }
                            break;
                            // for all other relationtypes, add to same layer
                        case "spouse":
                            currentLayer.push(pToAdd);
                            break;
                        default:
                            defaultLayer.push(pToAdd);
                    }
                    return;
                }
            }
        }
    };
    
    var removeFromTree = function(personId) {
        // find person in tree
        for (var i = 0; i < tree.length; i++) {
            for (var j = 0; j < tree[i].length; j++) {
                if (tree[i][j].id === personId) {
                    // found person, remove it
                    if (tree[i].length === 1) {
                        tree.splice(i, 1);     // only this person in layer, delete the layer
                    } else {
                        tree[i].splice(j, 1);  // delete person from layer
                    }
                    return;
                }
            }
        }
    };

    var _init = function(persons, connections){
        allPeople = persons;
        allEdges = connections;
        // Deep copy
        remainingPeople = $.extend(true,[],persons);
        edges = connections;
        defaultLayer = new Array();
        tree = new Array();
        
        doLayering();
    }
    
    return {
        // public functions
        init: function(persons, connections) {
            
            _init(persons, connections);
            // TESTS: METHODS
//            removeFromLayering([
//                {name: "GrandMother", surname: "Node", gender: "female", id: 1},
//                {name: "GrandFather", surname: "Node", gender: "male", id: 2},
//                {name: "GrandMother", surname: "Node", gender: "female", id: 3},
//                {name: "GrandFather", surname: "Node", gender: "male", id: 4}
//            ]);

//            alertTree();
            Restrictor.init(tree,edges);
        },
        reset: function(persons, connections){
            _init(persons, connections);

            for (var i = 0; i < persons.length; i++) {
                Restrictor.addPerson(persons[i]);
            };

            for (var i = 0; i < connections.length; i++) {
                Restrictor.addEdge(connections[i]);
            };

            Restrictor.restartRestrictions(tree);

        },
        add: function(persons, connections) {
            allPeople = allPeople.concat(persons);
            allEdges = allEdges.concat(connections);

            remainingPeople = $.extend(true,[],allPeople);
            edges = allEdges;

            for (var i = 0; i < persons.length; i++) {
                Restrictor.addPerson(persons[i]);
            };

            for (var i = 0; i < connections.length; i++) {
                Restrictor.addEdge(connections[i]);
            };

            defaultLayer = new Array();
            tree = new Array();
            
            doLayering();
            
            Restrictor.restartRestrictions(tree);
        },
        remove: function(persons, connections) {
            removeFromLayering(persons);
            removeFromConnections(connections);
            
            for (var i = 0; i < persons.length; i++) {
                Restrictor.removePerson(persons[i].id);
            };
            Restrictor.restartRestrictions(tree);
        },
        removePersonWithId: function(id) {
            removeFromTree(id);
            Restrictor.removePerson(id);
            Restrictor.restartRestrictions(tree);
        },
        getAllEdges: function(){
            return allEdges;
        },
        getAllPeople: function(){
            return allPeople;
        }
    };
}(); 