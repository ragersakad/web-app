/* Draws each step of the family graph.
 * It is in charge of creating the actual html nodes, and with the
 * help of the layouter keeps track of them and their positions.
 * Binds the cations of some of the buttons in the app.
 */

var Renderer = function(){

	// Stores the system module, which controls the rendering rate and stores
	// all the nodes and edges.
	var system;

	// References to the elements in which the rendering is done
	var main = $("#main");
	var graphCanvas = $("#graph-canvas");
	var canvas = $("#canvas");
	var context = canvas[0].getContext("2d");

	// This variable is set when the template has been loaded
	// asynchronously. No rendering happens until the template is
	// downloaded.
	var nodeTemplate = null;

	// Id to which the nodes are collapsing.
	var collapsingId;

	// Load the template to draw the nodes.
	$.ajax({
		url: "content/node.template",
		success: function(data){
			nodeTemplate=data;
		}
	});

	/*** handler function creators ***/

    var createConnectHandler = function(nodeId){
		return function(e){
			TreeController.connectClicked(nodeId);
			return false;
		}
	}
        
    var createDeleteHandler = function(nodeId){
		return function(e){
			console.log("deletors!");
			TreeController.deleteClicked(nodeId);
			return false;
		}
	}

	var createNodeHandler = function(nodeId){
		return function(){
			TreeController.nodeClicked(nodeId);
		}
	}


	// Render a single node, bind its buttons to the interface and
	// return a jquery object for it.
	var renderTemplate = function(person){
		// If template is null return null
        if (nodeTemplate === null) 
            return null;
		var jObject = $(tmpl(nodeTemplate, {person: person}));

		// Add handlers
		jObject.bind("click",createNodeHandler(person.id));
		jObject.find(".connect-link").bind("click",createConnectHandler(person.id));
		jObject.find(".delete-link").bind("click",createDeleteHandler(person.id));

        return jObject;
	};

	var createNode = function(arborNode){
		// Render the themplate into a jquery object
        var n = renderTemplate(arborNode.data.person);
        // If rederTemplate returns null no jqueryNode should
		// be added to the data of the arborNode.    
        if (n === null)
            return;
		// Using jquery append it to the graph-canvas
        graphCanvas.append(n);
		// Store the jqery reference inside the node
        arborNode.data.jqueryNode = n;
	};

	var redrawNode = function(arborNode){
		// Get the jqery node from the arborNode
		// Set the css properties for its top and left acordingly
		// If node doesnt have jqueryNode set, call create.
		jqueryNode = arborNode.data.jqueryNode;
        if (jqueryNode) {
        	//var p = system.toScreen(arborNode.p);
        	var p = arborNode.p;
            jqueryNode.css("top", p.y);
            jqueryNode.css("left", p.x);
        } else {
            createNode(arborNode);
        }
	};

	// Erase all edges from the canvas before redrawing them
	var cleanCanvas = function(){
		// Clean the whole canvas
		context.clearRect(0, 0, canvas.width(), canvas.height());

	}

	var drawEdge = function(edge){
		// With canvas path functions draw a line.
		// First get the positions of the connected nodes.
		
       	var nodeSrc = system.getNode(edge.source);
        var nodeDst = system.getNode(edge.target);

        var nodeSrcX = nodeSrc.p.x;
        var nodeSrcY = nodeSrc.p.y;

        var nodeDstX = nodeDst.p.x;
        var nodeDstY = nodeDst.p.y;

        var type = edge.data["type"];

        if ((type == "father" || type == "mother") && nodeSrcY < nodeDstY) {
            context.beginPath();
            context.moveTo(nodeSrcX + 100, nodeSrcY + 80);
            context.lineTo(nodeDstX + 100, nodeDstY);
            context.stroke();
        } else if (type == "spouse") {
            context.beginPath();
            if (nodeSrcX < nodeDstX){
            	context.moveTo(nodeSrcX + 200, nodeSrcY + 40);
            	context.lineTo(nodeDstX, nodeDstY + 40);
            }else{
            	context.moveTo(nodeDstX + 200, nodeDstY + 40);
            	context.lineTo(nodeSrcX, nodeSrcY + 40);
            }
            context.stroke();
        }


	}

	// When the nodes have collapsed to move to another tree
	// all nodes except the one where the tree is going to
	// move have to be deleted.
	var deleteExceptCollapsed = function(node){
		if(node.id != collapsingId){
			node.data.jqueryNode.remove();
			system.deleteNode(node.id);
		}
	}

	return {
		// Initialise the nodes
		init: function(s){
			system = s;
			system.eachNode(createNode);
		},
		// Nodes might have moved and the tree has to be updated.
		redraw: function(){
			system.eachNode(redrawNode);
			cleanCanvas();
			system.eachEdge(drawEdge);
		},
		// Move all nodes to the center. Returns a deferred object
		// that resolves when the animation is done.
		collapseToId: function(id){
			var deferred = new $.Deferred();
			collapsingId = id;
			cleanCanvas();
			system.getNode(id).data.jqueryNode.css("z-index","200");
			$("#graph-canvas .node-contents").animate({
				top: "140px",
				left: "400px"
			},
			function(){
				system.eachNode(deleteExceptCollapsed);
				var node = system.getNode(id);
				node.data.jqueryNode.css("z-index","1");
				node.p.x = 400;
				node.p.y = 140;
				deferred.resolve(true);
			});
			return $.when(deferred);
		}
	}

}();