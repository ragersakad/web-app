/* Substitute for arbor.js library. 
 * It is in charge of keeping the rendering loop going.
 * It is the central point of storage for the whole family tree plugin,
 * it actually doesn't do layouting, but stores elements positions and
 * lets others read and modify them.
 */

var Layouter = function(){

	// All nodes and all edges
	var nodes = {};
	var edges = [];

	// The renderer module, in which init and redraw will be called.
	var renderer = null;

	// Initial position for all nodes and delta that should be added to it
	// for every node created.
	var lastPosition = {x: 400, y: 140};
	var deltaAuto = {x: 0, y: 0};

	// Wether the simulation is running or not.
	var simulation = true;

	// Add to points of the form {x: NUMBER, y: NUMBER}
	var addPoints = function(p1,p2){
		return {
			x: p1.x + p2.x,
			y: p1.y + p2.y
		};
	}


	// Create a node object from an id an initial position and data to be stored.
	var newNode = function(id, p, data){
		return {
			id: id,
			data: data,
			p: p,
			getDstEdges: function(){
				var res = [];
				for (var i = 0; i < edges.length; i++) {
					//var edge = edges[i];
					if (edges[i].target == id){
						res.push(edges[i]);
					}
				};
				return res;
			}
		};
	}

	// Update a node with new id and new data.
	var mergeNode = function(id, data){
		var prev = nodes[id];
		prev.data = $.extend({},prev.data,data);
	}

	// Wrapper around the redraw function for safety
	var callRender = function(){
		if(renderer && simulation){
			renderer.redraw();
		}
	}

	/*** 
		Timers for drawing in a loop. 
		These two functions try to keep a stable
		1000ms/20ms = 50 frames per second drawing loop. 
		If the redraw function can't keep up it will wait.
	***/

	// Waits until the renderer variable is set and then starts the drawing loop.
	var startDrawing = function(){
		if (!renderer){
			setTimeout(startDrawing,20);
		}
		renderer.init(self);
		setTimeout(drawingLoop,20);
	}

	var prevRendering = Date.now();

	// function that calls itself on a regular basis
	// and calls the renderer function.
	var drawingLoop = function(){
		var currTime = Date.now();
		if (currTime - prevRendering > 20){
			callRender();
			prevRendering = currTime;
			setTimeout(drawingLoop, 1); // ALways call setTimeout to avoid stack overflow.
		}else{
			setTimeout(drawingLoop, currTime-prevRendering);
		}
	}


	// Mostly utilities for the other modules of the application.
	var self = {
		init: function(params){
			return self;
		},
		// Add a node to the graph being presented
		addNode: function(id, data, p){
			if (!nodes[id]){
				if (!p){
					p = addPoints(lastPosition,deltaAuto);
				}else{
					p = addPoints(p,deltaAuto);
				}
				nodes[id] = newNode(id, p, data);
			}else{
				mergeNode(id, data);
			}
		},
		// Add an edge to the graph being presented
		addEdge: function(id1, id2, data){
			edges.push({
				source: id1,
				target: id2,
				data: data
			});
		},
		// Loop through each node executing operation in them
		eachNode: function(operation){
			for(var id in nodes){
				operation(nodes[id]);
			}
		},
		// Loop through all the nodes executing operation in them.
		eachEdge: function(operation){
			for (var i = edges.length - 1; i >= 0; i--) {
				operation(edges[i]);
			};
		},
		// Change the renderer. Usually starts the rendering.
		setRenderer: function(r){
			renderer = r;
			startDrawing();
		},
		// Get a node by its id.
		getNode: function(id){
			return nodes[id];
		},
		// return all edges for wich a filter holds.
		filterEdges: function(filter){
			var newEdges = [];
			for (var i = edges.length - 1; i >= 0; i--) {
				if(filter(edges[i])){
					newEdges.push(edges[i]);
				}
			};
			edges = newEdges;	
		},
		// Delete a node from the graph.
		deleteNode: function(id){
			delete nodes[id];
			this.filterEdges(function(edge){
				if (edge.source == id || edge.target == id){
					return false;
				}
				return true;
			});
		},
		// Collapse all nodes to one id. This function is in this
		// module too because the simulation needs to be stopped
		// while the new animation takes place.
		collapseToId: function(id){
			// Avoid multiple calls while animation is not over
			if (!simulation){
				return;
			}
			simulation = false;
			return renderer.collapseToId(id).then(function(){
				simulation = true;
			});
		}

	};

	// The self variable is used in this module
	// to allow calling the public methods from the private ones.
	return self;

}();
