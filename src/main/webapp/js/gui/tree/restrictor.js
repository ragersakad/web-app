/* This module is the responsible for the actual location of the nodes in the graph.
 * It works with restrictions and preferences.
 * Restrictions must be resolved while preferenes can be ignored.
 * The two restriction implied are:
 * ** Nodes in the same layer must not collide horizontally 
 * ** Nodes in any layer must have a y position lower than the nodes
 *    in the layers above and greater thatn the ones in the layer below.
 *    
 * There is only one preference and it tryes to place nodes as close as
 * possible to their parents (without breaking restrictions). When many nodes want
 * to go to the same position they measure the average distance to the position
 * for the whole layer and try to balance the tree.
 *
 * This process could be improved in many ways and could be separated from the 
 * rendering loop to provide a smoother rendering.
 */

var Restrictor = function () {

	/** Log utilities **/
	// As many methods are called over and over on the tree
	// this uilities provide a way to log the first n calls
	// to vaoid flodding the developers console.

	var logging = 0;

	var log = function(sth){
		if (logging < 1000){
			console.log(sth);
			logging++;
		}
	}

	// Store all the restrictions and preferences as lists
	// ans as dictionaries of nodes involved in them.
	var restrictions = [];
	var restrictionsByNode = {};
	var preferences = [];
	var preferencesByNode = {};

	// Arbor js system (not anymore, now it is the layouter, but the api is the same).
	var sys;

	// When redraw is called by the system, the call gets intercepted by the restrictions
	// ans a step of all restrictions and preferences is applied before anything is rendered.
	var sysRedrawCalled = function(){

		// Check restrictions
		for (var i = restrictions.length - 1; i >= 0; i--) {
			var restriction = restrictions[i];
			switch(restriction.type){
				case "horizontal":
					checkHorizontalRestriction(restriction);
					break;
				case "layer":
					checkLayerRestriction(restriction);
					break;
			}
		};

		// Try prefernces
		for (var i = 0; i < preferences.length; i++) {
			tryHorizontalPreference(preferences[i]);
		};
	}

	// A wrapper around the actual renderer is built to allow checking
	// restrictions at every step of the rendering.
	var customRenderer = {
		init: function(system){
			sys = system;
			Renderer.init(system);
		},
		redraw: function(){
			sysRedrawCalled();
			Renderer.redraw();
		},
		collapseToId: function(id){
			// In this case we only have to clean our cached restrictions
			return Renderer.collapseToId(id).then(function(){
				restrictions = [];
				restrictionsByNode = {};
				preferences = [];
				preferencesByNode = {};
			});
		}
	}

	// helper function
	var sign = function(x){
		return (x > 0) - (x < 0);
	}

	/*** Restriction and preference creators ***/

	// Base restrictions creator method.
	var buildInitialRestrictions = function(layers, people){
		restrictions = [];
		restrictionsByNode = {};
		preferences = [];
		preferencesByNode = {};
		for (var i = 0; i < people.length; i++) {
			restrictionsByNode[people[i].id] = [];
		};

		buildLayerRestrictions(layers);
		buildHorizontalRestrictions(layers);
		buildHorizontalPreferences(layers, people);
	}

	// Create all vertical restrictions. A node should be below those in the upper layers
	// and above those in lower layers. This restrictions are built on a neighbouring
	// layer basis, to avoid creating unnecessary ones. 
	var buildLayerRestrictions = function(layers){
		for (var i = 1; i < layers.length; i++) {
			var currentLayer = layers[i];
			var prevLayer = layers[i-1];
			for (var j = currentLayer.length - 1; j >= 0; j--) {
				for (var k = prevLayer.length - 1; k >= 0; k--) {
					restrictions.push({
						type: "layer",
						above: prevLayer[k].id,
						below: currentLayer[j].id
					});
				};
			};
		};
	}

	// Create restrictions that prevent nodes in the same layer from colliding with each other.
	var buildHorizontalRestrictions = function(layers){
		// For all alyers
		for (var i = layers.length - 1; i >= 0; i--) {
			var layer = layers[i];
			// For all nodes in each layer
			for (var j = 0; j < layer.length; j++) {
				var node1 = layer[j];
				// For all nodes the node hasn't been associated with yet.
				for (var k = j+1; k < layer.length; k++) {
					// Create a restriction.
					var node2 = layer[k];
					var restriction = {
						type: "horizontal",
						node1: node1.id,
						node2: node2.id
					};
					restrictions.push(restriction);
					restrictionsByNode[node1.id].push(restriction);
					restrictionsByNode[node2.id].push(restriction);
				};
			};
		};
	}

	// Function that returns a preference function that can be called in each turn
	// to see where a node wants to be.
	var createParentsPreferenceFunction = function(parents){
		return function(){
			var avg = 0;
			for (var i = 0; i < parents.length; i++) {
				avg += sys.getNode(parents[i]).p.x;
			};
			return avg/parents.length;
		}
	}

	// Build all preferences. The only preference in the system right now
	// is the one specifiying nodes with parents want to be right below them.
	var buildHorizontalPreferences = function(layers, people){
		for (var i = 0; i < people.length; i++) {
			var node = sys.getNode(people[i].id);
			var edges = node.getDstEdges();
			var parents = [];
			for (var j = 0; j < edges.length; j++) {
				if (edges[j].data["type"] == "father" || edges[j].data["type"] == "mother"){
					parents.push(edges[j].source);
				}
			};
			if (parents.length > 0){
				var pref = {
					node: node.id,
					pos: createParentsPreferenceFunction(parents)
				};
				preferences.push(pref);
				preferencesByNode[node.id] = pref;
			}
		};
	}

	/*** Restriction and preference checkers ***/

	// Cehck vertical restriction and move nodes acordingly
	var checkLayerRestriction = function(restriction){
		var aboveNode = sys.getNode(restriction.above);
		var belowNode = sys.getNode(restriction.below);
		if (aboveNode.p.y > belowNode.p.y - 95){
			belowNode.p.y += 1;
			aboveNode.p.y -= 1;
		}
	}

	// Check horizontal restriction and move nodes acordingly (unless preventUpdate is set)
	var checkHorizontalRestriction = function(restriction, preventUpdate){
		var node1 = sys.getNode(restriction.node1);
		var node2 = sys.getNode(restriction.node2);
		var diff = Math.abs(node1.p.x - node2.p.x);
		if (diff > 220){
			return diff;
		}
		if (node1.p.x <= node2.p.x){
			if (preventUpdate){
				return diff;
			}
			node2.p.x += 1;
			node1.p.x -= 1;
		}else{
			if (preventUpdate){
				return diff;
			}
			node2.p.x -= 1;
			node1.p.x += 1;
		}
		return diff;
	}

	// Attepmt to move to where the node wants. This movement should be avoided
	// if it caouses a node to collide with another node that also wants to go to the
	// same place and is approximately the same distance away from its goal.
	// The motion is also prevented if the layer is somewhat stabilized, that is the
	// average of the distances from nodes to where they want to be is close to 0.
	var tryHorizontalPreference = function(preference){
		var node = sys.getNode(preference.node);
		var diff = node.p.x - preference.pos();
		// If it is already on its place.
		if (diff == 0){
			return;
		}
		var diffSign = sign(diff);
		var absDiff = Math.abs(diff);

		// The movement that is going to be caused is stored
		// in the update variable to allow undoing quickly.
		var update = 0;
		if (absDiff < 10){
			if (absDiff < 2){
				update -= diff;
			}
			update -= diffSign * 2;
		}else{
			update -= diffSign * 4;
		}
		node.p.x += update;

		// If nothing goes wrong the update shouldnt be reverted.
		var undo = false;
		var undoIfCollide = false;


		// Check if row has already been "balanced". Add all differences from
		// where nodes are to where they want to be.
		var restrictions = restrictionsByNode[preference.node];
		var totalDiff = diff;
		var prefNodes = 1;
		for (var i = 0; i < restrictions.length; i++) {
			var restriction = restrictions[i];
			var otherId = (restriction.node1 == preference.node)? restriction.node2 : restriction.node1;
			var otherPreference = preferencesByNode[otherId];
			if (otherPreference){
				var otherDiff = sys.getNode(otherId).p.x - otherPreference.pos();
				totalDiff += otherDiff;
				prefNodes += 1;
			}
		};

		// Divide the total sum by the number of nodes anañyzed to have 
		// a fair comparison to 0. In this case the undo should only be
		// prevented if there are nodes that collide with this one on the way.
		if (Math.abs(totalDiff / prefNodes) < 10){
			undoIfCollide = true;
		}

		// Check how the update affects other nodes
		for (var i = 0; i < restrictions.length && !undo ; i++) {
			var restriction = restrictions[i];
			var hResDiff = checkHorizontalRestriction(restriction,true);
			// If two nodes collide due to this update it doesn't mean necessarily that the update shouldn't be done.
			if ( hResDiff > 200 && hResDiff < 220 ){
				// They are colliding by a small ammount. Probably the updates fault.
				if (undoIfCollide){
					undo = true;
					continue;
				}

				// In case they are both moving to the same place (their update sign matches),
				// the update shouldnt be prevented.

				// In case the node this one is going to collide with is much closer to the goal than 
				// the updated node we don't prevent the update either to make then balance around their
				// desired position. Useful whith many children with the same parents.

				var otherId = (restriction.node1 == preference.node)? restriction.node2 : restriction.node1;
				var otherPreference = preferencesByNode[otherId];
				if (otherPreference){
					var otherDiff = sys.getNode(otherId).p.x - otherPreference.pos();
					if ( sign(otherDiff) == diffSign || Math.abs(diff) <= Math.abs(otherDiff) || Math.abs(Math.abs(diff) - Math.abs(otherDiff)) < 22 ){
						undo = true;
					}
				}
			}else if(hResDiff < 220){
				// They are colliding by a huge ammount. Probably the horizontal restrictions haven't
				// stabilized yet. The update should be undone.
				undo = true;
			}
		};
		if (undo) {
			node.p.x -= update;
		};
		
	}


	
	// Wrapper around the system's addNode function
	var createNode = function(person){
		sys.addNode(person.id, {person: person});
	}

	// Wrapper around the system's addEdge function
	var createEdge = function(edge){
		sys.addEdge(edge.source, edge.target, {type: edge.type});
	}

	return {
		init: function(layers, edges){

			// Initialize arbor
			sys = Layouter.init();
			sys.setRenderer(customRenderer);

			// Flatten the layers to get all people and create their nodes.
			var people = [];
			people = people.concat.apply(people, layers);
			for (var i = 0; i < people.length; i++) {
				createNode(people[i]);
			};

			// Create all edges
			for (var i = 0; i < edges.length; i++) {
				createEdge(edges[i]);
			};

			// Build restrictions			
			buildInitialRestrictions(layers, people);

		},
		// Rebuild the restrictions but don't reinitialise the system.
		restartRestrictions: function(layers){
			var people = [];
			people = people.concat.apply(people, layers);
			buildInitialRestrictions(layers, people);
		},
		removePerson: function(id){
			Layouter.deleteNode(id);
		},
		addPerson: function(person){
			console.log(person);
			createNode(person);
		},
		addEdge: function(edge){
			createEdge(edge);
		}
	}

}();