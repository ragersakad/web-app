// This file contains all the methods an actions needed for the GUI
// of the how to page to work properly.

// Sub app controlling all sub routes that
// belong to the howto section.
var SammyHowToApp = Sammy("#howToContainer", function(){

	this.get("#/howto",function(){
		// close popUp if necessary (back button has been pressed)
		HowToModal.modal('hide');
	});

	this.get("#/howto/popup",function(context){
		// Show the popUp
		HowToModal.modal('show');
	});

});

// Initialize the modal dialog in case it is going to be shown.
var HowToModal = $("#howToModal").modal({show: false});

// Run the application. No default route is needed as this file
// is only loaded if there is already a route.
SammyHowToApp.run();

// If the howto section disappears this app has to unbind itself
// in order to work properly when this section is visited again.
SammyGlobalApp.bind("page-change",function(){
	SammyHowToApp.unload();
});

// Dirty trick to change back the url from #/howto/popup
// to #/howto when the modal has been hidden.
// For it to work a hidden button with id howToModalCloseButton
// has to be placed in the html.
HowToModal.on("hidden.bs.modal",function(){
	$("#howToModalCloseButton").click();
});

