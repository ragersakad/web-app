/* File for the creating user page.
 * It contains code for checking the form
 * and displaying its status to the user,
 * and a submit function to be called by Sammy.
 */


// Public function
var createUser = function(username,password){

	var result = $.Deferred();
	User.create(username,password,{
		success: function(){
			result.resolve(true);
		},
		error: function(){
			result.resolve(false);
		}
	});
	return $.when(result);
};

// Everything wrapped in a function to make 
// functions and variables private
var createdUserUI = function(){

	var userCorrect = false;
	var passwordCorrect = false;

	// Enable the submit button once all fields
	// have valid information. This doesn't mean
	// we don't need validation on the server.
	function finalCheck(){
		var button = $("#createUserButton");
		if(userCorrect && passwordCorrect){
			button.removeClass("disabled");
			button.addClass("btn-success");
		}else{
			button.removeClass("btn-success");
			button.addClass("disabled");
		}
	}

	// Prevent form from being submitted through other methods
	// like the enter key when the fields are not correct.
	$("#new-user-form").submit(function(e) {
		if (userCorrect && passwordCorrect){
		}else{
			// Return false on a jquery event handler prevents
			// the event from bubbling up and the default behaviour.
			return false;
		}
	});

	// Change the appearance of the username input.
	function setUsernameInputState(state){
		userCorrect = false;
		var element = $("#usernameFormGroup");
		element.removeClass("has-success");
		element.removeClass("has-loading");
		element.removeClass("has-error");
		element.removeClass("has-warning");
		switch(state){
			case "valid":
				element.addClass("has-success");
				userCorrect = true;
				break;
			case "checking":
				element.addClass("has-loading");
				break;
			case "taken":
				element.addClass("has-error");
				break;
			case "warning":
				element.addClass("has-warning");
				break;
			default:
				break;
		}
		finalCheck();
	}

	// Change the appearance of the password inputs.
	function setPasswordInputState(state){
		passwordCorrect = false;
		var second = $("#secondPasswordFormGroup");
		var first = $("#firstPasswordFormGroup");
		second.removeClass("has-success");
		second.removeClass("has-loading");
		second.removeClass("has-error");
		second.removeClass("has-warning");
		first.removeClass("has-success");
		first.removeClass("has-loading");
		first.removeClass("has-error");
		first.removeClass("has-warning");
		switch(state){
			case "valid":
				first.addClass("has-success");
				second.addClass("has-success");
				passwordCorrect = true;
				break;
			case "checking":
				first.addClass("has-loading");
				second.addClass("has-loading");
				break;
			case "short":
				first.addClass("has-error");
				break;
			case "nomatch":
				first.addClass("has-success");
				second.addClass("has-error");
				break;
			case "warning":
				first.addClass("has-warning");
				second.addClass("has-warning");
				break;
			default:
				break;
		}
		finalCheck();
	}

	// Check if the username is available.
	function checkUsername(){
		var username = $("#userInput").val();
		if (!username){
			setUsernameInputState("undefined");
			return;
		}
		User.checkUsernameAvailable(username,{
			success: function(data){
				if (data.value) {
					setUsernameInputState("valid");
				}else{
					setUsernameInputState("taken");
				}
			},
			error: function(){
				setUsernameInputState("warning");
			}
		});
	}

	// Check if the password is long enought and if it
	// matches the repetition field.
	function checkPassword(){
		var password1 =  $('#passwordInput').val();
		var password2 =  $('#repeatPasswordInput').val();
		if(!password1 || password1.length < 6){
			setPasswordInputState("short");
		}else{
			if (password2 != password1){
				setPasswordInputState("nomatch");
			}else{
				setPasswordInputState("valid");
			}
		}
	}

	// Timer to prevent checking username at every keystroke
	var timerUser = null;
	$('#userInput').keyup(function(){
		setUsernameInputState("checking");
		clearTimeout(timerUser);
		timerUser = setTimeout(checkUsername, 1000);
	});

	// Timer to prevent checking password at every keystroke
	var timerPassword = null;
	$('#passwordInput, #repeatPasswordInput').keyup(function(){
		setPasswordInputState("checking");
		clearTimeout(timerPassword);
		timerPassword = setTimeout(checkPassword, 250);
	});

	// Return that everything has been loaded correctly.
	return true;

}();
