// This file contains general UI functions for the navigation on the website.

// Deselects all the links in the navigation bar.
function clearLinks(){
	$("#navigation-links > li").removeClass("active");
}

// Changes the selected element in the navigaton bar to the one
// indicated by the argument dst.
function changeSection(dst){
	clearLinks();
	$("#"+dst+"-link").addClass("active");
}

// Bind all click events of <a> tags so that when clicked 
// inmediatly loose focus and don't stay with grey dots
// around them.
$(document).on("click","a, button",function(){
	this.blur();
});

// Chenge the string in the top right of the page to
// the one displayed when the user isn't logged in.
function setLoggedOutString(){
	$("#session-state-text").html("Annonymous user: <a href=\"#/login\">Log in</a> or <a href=\"#/createUser\">create an account</a>");
}

// Change the string displayed in the top right of the page
// to the one displayed when the user is logged in
function setLoggedInString(user){
	$("#session-state-text").html("Logged in as <a href=\"#/user\">"+user.username+"</a>. <a href=\"#/logout\">Log out</a>");
}

function setLoginLoadingString(){
	$("#session-state-text").html("Loading...&nbsp;&nbsp; <span class=\"glyphicon glyphicon-refresh icon-spin\" ></span>");	
}