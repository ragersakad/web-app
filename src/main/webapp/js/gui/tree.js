// This file contains all the methods an actions needed for the GUI
// of the tree page to work properly. That is the forms that are 
// included in the template, and there is only one, the create person
// one. This is done inside a function to prevent flodding the global
// context with useless functions and variables.

// Initialize the graph creator on the tree-graph div. The code for the actual rendering is inside this jquery plugin.
$("#tree-graph").graphCreator();


//Create person form methods
var createdNewPersonUI = function(){

	var nameCorrect = false;
	var surnameCorrect = false;

	// Enable the submit button once all fields
	// have valid information. This doesn't mean
	// we don't need validation on the server.
	function finalCheck(){
		var button = $("#personCreateButton");
		if(nameCorrect && surnameCorrect){
			button.removeClass("disabled");
			button.addClass("btn-success");
		}else{
			button.removeClass("btn-success");
			button.addClass("disabled");
		}
	}

	// Return a person object with all the data from the form
	function retrieveData(){
		return {
			name: $("#inputName").val(),
			surname: $("#inputSurname").val(),
			birthdate: $("#inputBirthDate").val(),
			deathdate: $("#inputDeathDate").val(),
			gender: $( "input:radio[name=genderOption]:checked" ).val()
		};
	}

	// Prevent form from being submitted through other methods
	// like the enter key when the fields are not correct.
	$("#new-person-form").submit(function(e) {
		if (nameCorrect && surnameCorrect && datesCorrect()){
			TreeController.createPersonFormSubmitted(retrieveData());	
			return false;
		}else{
			// Return false on a jquery event handler prevents
			// the event from bubbling up and the default behaviour.
			return false;
		}
	});

	// Check if the name entered is valid.
	function checkName(){
		var name = $("#inputName").val();
		if (!name){
			nameCorrect = false;
		}else{
			nameCorrect = true;
		}
	}

	// Check if the username entered is valid
	function checkSurname(){
		var surname = $("#inputSurname").val();
		if (!surname){
			surnameCorrect = false;
		}else{
			surnameCorrect = true;
		}
	}

	// Check if the dates selected are correct.
	// At least birth date has to be specified
	// and it should be smaller than death date.
	function datesCorrect(){
		var sdate1 = $("#inputBirthDate").val();
		var sdate2 = $("#inputDeathDate").val();
		if (sdate1 && sdate2){
			var date1 = new Date(sdate1);
			var date2 = new Date(sdate2);
			if (date2 - date1 >= 0) {
				return true
			}else{
				alert("Death date has to be after birth date.");
			}
		}else if(sdate1 && !sdate2){
			return true;
		}else{
			alert("You should specify birth date at least.");
		}
		return false;
	}

	// Cehck name when a key is pressed inside the input
	$("#inputName").keyup(function(){
		checkName();
		finalCheck();
	});

	// Cehck surname when a key is pressed inside the input
	$("#inputSurname").keyup(function(){
		checkSurname();
		finalCheck();
	});


	// Return that everything has been loaded correctly.
	return true;

};