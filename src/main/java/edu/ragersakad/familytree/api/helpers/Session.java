package edu.ragersakad.familytree.api.helpers;


import edu.ragersakad.familytree.core.User;
import edu.ragersakad.familytree.db.utils.AbstractEntity;

import javax.persistence.*;
import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 13:13
 *
 * Entity stored in the database that represents a user session.
 * It is identified by a token which is given to the user to authenticate
 * future requests to the api. This token is generated randomly here.
 */
@Entity
@Table(name = "SESSIONS")
public class Session extends AbstractEntity{

    @Column(unique = true,nullable = false)
    private String token;
    @OneToOne(optional = false)
    private User user;

    public Session() {
    }

    public Session(User user) {
        this.token = Session.nextSessionToken();
        this.user = user;
    }

    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User userId) {
        this.user = userId;
    }

    // Random token generator

    private static SecureRandom random = new SecureRandom();

    public static String nextSessionToken()
    {
        return new BigInteger(130, random).toString(32);
    }

}
