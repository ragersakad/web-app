package edu.ragersakad.familytree.api.helpers;

import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.server.impl.inject.AbstractHttpContextInjectable;
import com.sun.jersey.spi.inject.Injectable;
import com.sun.jersey.spi.inject.PerRequestTypeInjectableProvider;
import edu.ragersakad.familytree.core.User;
import edu.ragersakad.familytree.db.DBHelper;

import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 23:45
 *
 * A class that adds the ability to use @Context User user to the parameters
 * of any resource method and will contain the user making the request (if the
 * request is authenticated). If it isn't it will return null.
 *
 * This is useful for knowing the author of a request (for logging purposes) or
 * for checking admin rights for some methods (if we had admins).
 *
 * Unfortunately there is no way of using the @Context session here and avoid fetching the
 * session from the database again.
 */

@Provider
public class UserProvider extends PerRequestTypeInjectableProvider<Context,User>{

    public UserProvider() {
        super(User.class);
    }

    @Override
    public Injectable<User> getInjectable(ComponentContext componentContext, Context context) {
        return new AbstractHttpContextInjectable<User>() {
            @Override
            public User getValue(HttpContext httpContext) {
                String token = httpContext.getRequest().getHeaderValue("x-session-token");
                // return null if no user logged in
                if (token == null){
                    return null;
                }
                Session s = DBHelper.sessions().findByToken(token);
                if (s == null)
                    return null;
                return s.getUser();
            }
        };
    }
}
