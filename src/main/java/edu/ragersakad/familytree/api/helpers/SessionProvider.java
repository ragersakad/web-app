package edu.ragersakad.familytree.api.helpers;

import com.sun.jersey.api.core.HttpContext;
import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.server.impl.inject.AbstractHttpContextInjectable;
import com.sun.jersey.spi.inject.Injectable;
import com.sun.jersey.spi.inject.PerRequestTypeInjectableProvider;
import edu.ragersakad.familytree.db.DBHelper;


import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 23:45
 *
 * A class that adds the possibility to use @Context Session session
 * in any resource in the application. It will return null if no session is
 * active and a Session object if there is.
 */

@Provider
public class SessionProvider extends PerRequestTypeInjectableProvider<Context,Session>{

    public SessionProvider() {
        super(Session.class);
    }

    @Override
    public Injectable<Session> getInjectable(ComponentContext componentContext, Context context) {
        return new AbstractHttpContextInjectable<Session>() {
            @Override
            public Session getValue(HttpContext httpContext) {
                String token = httpContext.getRequest().getHeaderValue("x-session-token");
                return DBHelper.sessions().findByToken(token);
            }
        };
    }
}
