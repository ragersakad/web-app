package edu.ragersakad.familytree.api.helpers;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import edu.chl.hajo.wss.PrimitiveJSONWrapper;
import edu.ragersakad.familytree.db.DBHelper;

import javax.ws.rs.WebApplicationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 16:05
 *
 * This file provides a jersey filter that can be applied to any resource
 * or resource method to prevent it from being accessed if the user hasn't
 * been authenticated.
 *
 * If that's the case the usual request cycle is interrupted and 403 response
 * with some json is returned to the caller.
 */

// This could be done with two classes but we are using only one for simplicity.
public class AuthFilter implements ContainerRequestFilter, ResourceFilter{

    @Override
    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    // We only need to filter the request, not the response.
    @Override
    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    // The actual filter.
    @Override
    public ContainerRequest filter(ContainerRequest containerRequest) {
        /* Unfortunately the @Context Session helper cannot be used here
         * as jax rs does not provide the context in the filters.
         */

        // Get the token from the requests headers.
        String token = containerRequest.getHeaderValue("x-session-token");
        if (token != null && !token.isEmpty()){
            Session s = fetchSession(token);
            if (s != null){
                return containerRequest;
            }
        }
        // Jersey filters allow a custom response in a filter by throwing an exception.
        throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).type(MediaType.APPLICATION_JSON).entity(new PrimitiveJSONWrapper<>("This resource requires login")).build());
    }

    // Access the database to find if the session exists.
    Session fetchSession(String token){
        return DBHelper.sessions().findByToken(token);
    }
}
