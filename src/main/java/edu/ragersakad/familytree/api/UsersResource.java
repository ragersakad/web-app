package edu.ragersakad.familytree.api;

import com.sun.jersey.spi.container.ResourceFilters;
import edu.chl.hajo.wss.PrimitiveJSONWrapper;
import edu.ragersakad.familytree.api.helpers.AuthFilter;
import edu.ragersakad.familytree.api.proxies.UserProxy;
import edu.ragersakad.familytree.core.User;
import edu.ragersakad.familytree.db.DBHelper;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 22:41
 *
 * API Description at: http://docs.familytree.apiary.io/
 */

@Path("users/")
@ResourceFilters({AuthFilter.class})
@Produces(MediaType.APPLICATION_JSON)
public class UsersResource {

    @GET
    public Response getAllUsers(){
        return null;
    }

    @GET
    @Path("{username}")
    public Response getUser(@PathParam("username") String username){
        User u = DBHelper.users().findByUsername(username);
        if (u == null){
            return Response.status(Response.Status.NOT_FOUND).entity(new PrimitiveJSONWrapper<>("User "+username+" not found")).build();
        }
        return Response.ok(u).build();
    }

    @POST
    @ResourceFilters({})
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response newUser(@FormParam("username") String username, @FormParam("password") String password){
        if (!checkUsernameAvailable(username)){
            return Response.status(Response.Status.CONFLICT).entity(new PrimitiveJSONWrapper<>("Username already taken")).build();
        }
        User u = new User();
        u.setUsername(username);
        u.setPassword(password);
        DBHelper.users().add(u);
        return Response.created(URI.create("/api/users/"+username)).entity(new PrimitiveJSONWrapper<>(username)).build();
    }

    @PUT
    @Path("{username}")
    public Response updateUser(@PathParam("username") String username, @Context User user,
                               @FormParam("new-username") String newUsername,
                               @FormParam("old-password") String oldPassword,
                               @FormParam("new-password") String newPassword){
        if (!user.getUsername().toUpperCase().equals(username.toUpperCase())){
            return Response.status(Response.Status.BAD_REQUEST).entity(new PrimitiveJSONWrapper<>("You can only update your own user")).build();
        }
        String error = null;
        if (newUsername != null){
            // Possible race condition. Luckily there is one more check in the database constrains.
            if (checkUsernameAvailable(newUsername)){
                user.setUsername(newUsername);
            }else{
                error = "Requested username not available";
            }
        }

        if (oldPassword != null && newPassword != null && error == null){
            if (user.checkPassword(oldPassword)){
                user.setPassword(newPassword);
            }else{
                error = "Incorrect old password";
            }
        }

        if (error == null){
            DBHelper.users().update(user);
            return Response.status(Response.Status.CREATED).entity(new UserProxy(user)).build();
        }else{
            return Response.status(Response.Status.BAD_REQUEST).entity(new PrimitiveJSONWrapper<>(error)).build();
        }
    }

    @DELETE
    @Path("{username}")
    public Response deleteUser(@PathParam("username") String username, @Context User user){
        if (!user.getUsername().toUpperCase().equals(username.toUpperCase())){
            return Response.status(Response.Status.BAD_REQUEST).entity(new PrimitiveJSONWrapper<>("You can only delete your own user")).build();
        }
        DBHelper.users().remove(user.getId());
        return Response.ok().build();
    }

    @GET
    @Path("available/{username}")
    @ResourceFilters({})
    public Response checkUsername(@PathParam("username") String username){
        return Response.ok(new PrimitiveJSONWrapper<>(checkUsernameAvailable(username))).build();
    }

    private boolean checkUsernameAvailable(String username){
        return null == DBHelper.users().findByUsername(username);
    }

}
