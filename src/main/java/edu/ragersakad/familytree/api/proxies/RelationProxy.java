/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ragersakad.familytree.api.proxies;

import edu.ragersakad.familytree.core.Relation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Cecilia Schmidt
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "Relation", propOrder = {
    "source", 
    "target", 
    "type"})
public class RelationProxy {
    Relation relation;

    public RelationProxy() {
    }

    public RelationProxy(Relation r) {
        this.relation = r;
    }
    
    @XmlElement(required=true)
    public Long getSource() {
        return relation.getFirst().getId();
    }
    
    @XmlElement(required=true)
    public Long getTarget() {
        return relation.getOther().getId();
    }
    
    @XmlElement(required=true)
    public String getType() {
        return relation.getType().getName();
    }
}
