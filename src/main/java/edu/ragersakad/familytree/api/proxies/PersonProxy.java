/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ragersakad.familytree.api.proxies;

import edu.ragersakad.familytree.core.Person;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ahmed Sheikh
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "Person", propOrder = {
    "id", 
    "name", 
    "surname", 
    "gender", 
    "birthdate", 
    "deathdate"})
public class PersonProxy {

    Person person;

    public PersonProxy() {
    }

    public PersonProxy(Person person) {
        this.person = person;
    }

    @XmlElement(required = true)
    public Long getId() {
        return person.getId();
    }
    
    @XmlElement(required = true)
    public String getName() {
        return person.getFirstName();
    }

    @XmlElement(required = true)
    public String getSurname() {
        return person.getLastName();
    }
    @XmlElement(required=true)
    public String getGender(){
        return person.getGender().toString().toLowerCase();
        
    }
    @XmlElement(required=true)
    public Date getBirthdate(){
        return person.getBirthDate();
    }
     @XmlElement(required=true)
    public Date getDeathdate(){
        return person.getDeathDate();
    }
}
