package edu.ragersakad.familytree.api.proxies;

import edu.ragersakad.familytree.core.User;

import javax.xml.bind.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 02:31
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "User", propOrder = {
        "username",
        "rootid"
})
public class UserProxy {

    private User user;

    public UserProxy() {
    }

    public UserProxy(User user) {
        this.user = user;
    }

    @XmlElement(required=true)
    public String getUsername() {
        return user.getUsername();
    }
    
    @XmlElement(required=true)
    public Long getRootid() {
        if (user.getRootPerson() != null)
            return user.getRootPerson().getId();
        return null;
    }
}
