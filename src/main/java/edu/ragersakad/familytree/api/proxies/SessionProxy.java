package edu.ragersakad.familytree.api.proxies;

import edu.ragersakad.familytree.api.helpers.Session;

import javax.xml.bind.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 02:31
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "Session", propOrder = {
        "token",
        "user"
})
public class SessionProxy {

    private Session session;

    public SessionProxy() {
    }

    public SessionProxy(Session session) {
        this.session = session;
    }

    @XmlElement(required=true)
    public String getToken() {
        return session.getToken();
    }

    @XmlElement(required=true)
    public UserProxy getUser() {
        return new UserProxy(session.getUser());
    }
}
