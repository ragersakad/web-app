package edu.ragersakad.familytree.api;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 01:39
 *
 * API Description at: http://docs.familytree.apiary.io/
 */


import com.sun.jersey.spi.container.ResourceFilters;
import edu.chl.hajo.wss.PrimitiveJSONWrapper;
import edu.ragersakad.familytree.api.helpers.AuthFilter;
import edu.ragersakad.familytree.db.DBHelper;
import edu.ragersakad.familytree.api.helpers.Session;
import edu.ragersakad.familytree.api.proxies.SessionProxy;
import edu.ragersakad.familytree.core.User;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("sessions/")
@Produces(MediaType.APPLICATION_JSON)
public class SessionsResource {


    @GET
    @Path("{token}")
    @ResourceFilters({AuthFilter.class})
    public Response getSessionInfo(@PathParam("token") String token, @Context Session session){
        if (session != null){
            if (!token.equals(session.getToken())){
                return Response.status(Response.Status.BAD_REQUEST).entity(new PrimitiveJSONWrapper<>("Token requested does not match session")).build();
            }
            return Response.ok(new SessionProxy(session)).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createSession(@FormParam("username") String username, @FormParam("password") String password){

        User u = DBHelper.users().findByUsername(username);
        if (u != null){
            if (u.checkPassword(password)){
                // Delete previous sessions
                Session prevSession = DBHelper.sessions().findByUser(u);
                if(prevSession != null){
                    DBHelper.sessions().remove(prevSession.getId());
                }
                // Create a new one.
                Session s = new Session(u);
                DBHelper.sessions().add(s);
                return Response.ok(new SessionProxy(s)).build();
            } else {
                return Response.status(Response.Status.FORBIDDEN).entity(new PrimitiveJSONWrapper<>("Wrong password")).build();
            }
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity(new PrimitiveJSONWrapper<>("User not found")).build();
        }
    }

    @DELETE
    @Path("{token}")
    @ResourceFilters({AuthFilter.class})
    public Response deleteSession(@PathParam("token") String token, @Context Session session){
        if (!token.equals(session.getToken())){
            return Response.status(Response.Status.BAD_REQUEST).entity(new PrimitiveJSONWrapper<>("Token requested does not match session")).build();
        }
        DBHelper.sessions().deleteByToken(token);
        return Response.ok().build();
    }
}
