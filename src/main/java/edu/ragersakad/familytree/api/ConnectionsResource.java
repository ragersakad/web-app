package edu.ragersakad.familytree.api;

import edu.chl.hajo.wss.PrimitiveJSONWrapper;
import edu.ragersakad.familytree.api.proxies.RelationProxy;
import edu.ragersakad.familytree.core.Relation;
import edu.ragersakad.familytree.db.DBHelper;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 10/1/13
 * Time: 23:11
 *
 * API Description at: http://docs.familytree.apiary.io/
 */
@Produces(MediaType.APPLICATION_JSON)
public class ConnectionsResource {

    @GET
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getConnectionsForPerson(@PathParam("id") Long id){
        // TODO fix this to be able to get more layers
        List<Relation> relations = DBHelper.familytree().getRelations(id);
        List<RelationProxy> rpx = new ArrayList<>();
        for (Relation r : relations) {
            rpx.add(new RelationProxy(r));
        }
        GenericEntity<List<RelationProxy>> ge = new GenericEntity<List<RelationProxy>>(rpx) {
        };
        return Response.ok(ge).build();
    }

    @POST
    @Path("{src}/{dst}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createConnection(@PathParam("src") Long srcId, @PathParam("dst") Long dstId,
                                     @FormParam("type") String type){
        Relation.RelationType t = Relation.RelationType.fromString(type);
        // check if valid RelationType enum
        if (t == null) {
            return Response.status(Response.Status.NOT_FOUND).entity(new PrimitiveJSONWrapper<>("Invalid RelationType: " + type)).build();
        }
        DBHelper.familytree().createRelation(srcId, dstId, t);
        return Response.ok(new PrimitiveJSONWrapper<>("Connection created successfully")).build();
    }

    @DELETE
    @Path("{src}/{dst}")
    public Response deleteConnection(@PathParam("src") Long srcId, @PathParam("dst") Long dstId){
        DBHelper.familytree().deleteRelation(srcId, dstId);
        return Response.ok().build();
    }
}
