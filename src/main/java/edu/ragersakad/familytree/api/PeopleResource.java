package edu.ragersakad.familytree.api;

import com.sun.jersey.spi.container.ResourceFilters;
import edu.chl.hajo.wss.PrimitiveJSONWrapper;
import edu.ragersakad.familytree.api.helpers.AuthFilter;
import edu.ragersakad.familytree.api.proxies.PersonProxy;
import edu.ragersakad.familytree.core.Person;
import edu.ragersakad.familytree.core.User;
import edu.ragersakad.familytree.db.DBHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Created with IntelliJ IDEA. User: guillermo Date: 10/1/13 Time: 22:59
 *
 * API Description at: http://docs.familytree.apiary.io/
 */
@Path("people/")
@Produces(MediaType.APPLICATION_JSON)
public class PeopleResource {

    @GET
    @Path("{id}")
    public Response getById(@PathParam("id") Long id) {
        Person p = DBHelper.familytree().getById(id);
        if (p == null) {
            Logger.getAnonymousLogger().log(Level.INFO, "PeopleResource.getById not found");
            return Response.status(Response.Status.NOT_FOUND).entity(new PrimitiveJSONWrapper<>("Person with id " + id + " not found")).build();
        } else {
            Logger.getAnonymousLogger().log(Level.INFO, "PeopleResource.getById found: " + p.toString());
            PersonProxy px = new PersonProxy(p);
            return Response.ok(px).build();

        }
    }

    @GET
    @Path("name/{name}")
    public Response getByName(@PathParam("name") String name) {
        List<Person> listOfPeople = DBHelper.familytree().getByName(name);
        if (listOfPeople.isEmpty()) {
            return Response.noContent().entity(new PrimitiveJSONWrapper<>("No people with name '" + name + "' were found")).build();
        } else {
            List<PersonProxy> ppx = new ArrayList<>();
            for (Person p : listOfPeople) {
                ppx.add(new PersonProxy(p));
            }
            GenericEntity<List<PersonProxy>> ge = new GenericEntity<List<PersonProxy>>(ppx) {};
            return Response.ok(ge).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @ResourceFilters({AuthFilter.class})
    public Response createNewPerson(@FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName, @FormParam("gender") String gender,
            @FormParam("birthDate") Date birthDate, @FormParam("deathDate") Date deathDate, @Context User creator) {
       

        Person newPerson = DBHelper.familytree().addPerson(firstName, lastName, Person.Gender.valueOf(gender.toUpperCase()), birthDate, deathDate);
        Logger.getAnonymousLogger().log(Level.INFO, "PeopleResource.createNewPerson: " + newPerson.toString() + "\n Created by: "+creator.toString());

        // if this is the users first created person, set the person as users root person
        if (creator.getRootPerson() == null) {
            creator.setRootPerson(newPerson);
            DBHelper.users().update(creator);
        }
        
        return Response.ok(newPerson.getId()).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @ResourceFilters({AuthFilter.class})
    public Response updatePerson(@PathParam("id") Long id, @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName, @FormParam("gender") String gender,
            @FormParam("birthDate") Date birthDate, @FormParam("deathDate") Date deathDate) {

        Person updatePerson = DBHelper.familytree().updatePerson(id, firstName, lastName, Person.Gender.valueOf(gender.toUpperCase()), birthDate, deathDate);
        if (updatePerson == null) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(new PersonProxy(updatePerson)).build();
    }

    @DELETE
    @Path("{id}")
    @ResourceFilters({AuthFilter.class})
    public Response deletePerson(@PathParam("id") Long id) {
        if (id == null) {
            return Response.status(Response.Status.BAD_REQUEST).entity(new PrimitiveJSONWrapper<>("Requested id is null")).build();
        } else {
            DBHelper.familytree().deletePerson(id);
            return Response.ok().build();
        }
    }
}
