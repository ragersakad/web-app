package edu.ragersakad.familytree.api;

import edu.chl.hajo.wss.PrimitiveJSONWrapper;
import edu.ragersakad.familytree.core.Person;
import edu.ragersakad.familytree.core.User;
import edu.ragersakad.familytree.db.DBHelper;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 10/1/13
 * Time: 23:08
 *
 * API Description at: http://docs.familytree.apiary.io/
 */

@Path("tree/")
@Produces(MediaType.APPLICATION_JSON)
public class TreeResource {

    @Path("connections")
    public ConnectionsResource getConnections(){
        return new ConnectionsResource();
    }

    @GET
    @Path("root")
    public Response getTreeRoot(@Context User user){
        Person p;
        // If no session then return random
        if (user == null) {
            p = DBHelper.familytree().getRandomPerson();
        } else if (user.getRootPerson() == null) {
           p = null;
        } else {
            p = DBHelper.familytree().getById(user.getRootPerson().getId());
        }

        if (p == null){
            return Response.status(Response.Status.NOT_FOUND).entity(new PrimitiveJSONWrapper<>("There is no root yet for this user")).build();
        }
        
        return Response.ok(new PrimitiveJSONWrapper<>(p.getId())).build();
    }
}
