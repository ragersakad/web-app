package edu.ragersakad.familytree.db;

import edu.ragersakad.familytree.core.FamilyTree;
import edu.ragersakad.familytree.core.IFamilyTree;
import edu.ragersakad.familytree.db.DAOs.SessionDao;
import edu.ragersakad.familytree.db.DAOs.UserDao;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA. User: guillermo Date: 9/30/13 Time: 22:31
 */
public class DBHelper {

    private static final String DEFAULT_PU = "familytree_pu";
    private static boolean initialized = false;
    private static SessionDao sessions;
    private static UserDao users;
    private static IFamilyTree familytree;

    public static void initialize(String persistenceUnitName) {
        if (initialized) {
            return;
        }

        Logger.getAnonymousLogger().log(Level.INFO, "initialized DBHelper");
        sessions = new SessionDao(persistenceUnitName);
        users = new UserDao(persistenceUnitName);
        familytree = new FamilyTree(persistenceUnitName);
        //TODO: Place this better
        initialized = true;
    }

    public static SessionDao sessions() {
        initialize(DEFAULT_PU);
        return sessions;
    }

    public static UserDao users() {
        initialize(DEFAULT_PU);
        return users;
    }
    public static IFamilyTree familytree(){
        initialize(DEFAULT_PU);
        return familytree;
    }
}
