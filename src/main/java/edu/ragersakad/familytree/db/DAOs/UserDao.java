package edu.ragersakad.familytree.db.DAOs;

import edu.ragersakad.familytree.core.User;
import edu.ragersakad.familytree.db.utils.AbstractDAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/30/13
 * Time: 21:30
 */
public class UserDao extends AbstractDAO<User, Long>{

    public UserDao(String puName) {
        super(User.class, puName);
    }

    public User findByUsername(String username){
        try {
            EntityManager em = this.getEmf().createEntityManager();
            return em.createQuery("SELECT u FROM User u WHERE UPPER(u.username) = UPPER('"+username+"') ",User.class).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }
}
