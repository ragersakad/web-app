package edu.ragersakad.familytree.db.DAOs;

import edu.ragersakad.familytree.api.helpers.Session;
import edu.ragersakad.familytree.core.User;
import edu.ragersakad.familytree.db.utils.AbstractDAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/30/13
 * Time: 21:30
 */
public class SessionDao extends AbstractDAO<Session, Long>{

    public SessionDao( String puName) {
        super(Session.class, puName);
    }

    public Session findByToken(String token){
        try{
            EntityManager em = this.getEmf().createEntityManager();
            return em.createQuery("SELECT s FROM Session s WHERE s.token = '"+token+"'",Session.class).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }

    public void deleteByToken(String token){
        EntityManager em = this.getEmf().createEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE FROM Session s WHERE s.token = '"+token+"'").executeUpdate();
        em.getTransaction().commit();
    }

    public Session findByUser(User u) {
        try{
            EntityManager em = this.getEmf().createEntityManager();
            return em.createQuery("SELECT s FROM Session s WHERE s.user = (:u)", Session.class)
                    .setParameter("u",u).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }
}
