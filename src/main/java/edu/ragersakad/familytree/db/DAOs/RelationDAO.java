package edu.ragersakad.familytree.db.DAOs;

import edu.ragersakad.familytree.core.Person;
import edu.ragersakad.familytree.core.Relation;
import edu.ragersakad.familytree.db.utils.AbstractDAO;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * Handles database for relations.
 * 
 * @author Cecilia Schmidt
 */
public final class RelationDAO extends AbstractDAO<Relation, Long>
        implements IRelationDAO {
    
    public RelationDAO(String puName) {
        super(Relation.class, puName);
    }
    // Factory method
    public static IRelationDAO newInstance(String puName) {
        return new RelationDAO(puName);
    }
    
    
    /**
     * Generic add method for relations. To avoid errors when adding parents, 
     * use the other add methods!
     *
     * @param first
     * @param other
     * @param type the relation type
     */
    @Override
    public void addRelation(Person first, Person other, Relation.RelationType type) {
        add(new Relation(first, other, type));
    }
    
    // keeping methods for parental relations since these are directed and might be tricky
    @Override
    public void addFatherChild(Person father, Person child) {
        add(new Relation(father, child, Relation.RelationType.FATHER));
    }
    
    @Override
    public void addMotherChild(Person mother, Person child) {
        add(new Relation(mother, child, Relation.RelationType.MOTHER));
    }
    
    
    @Override
    public List<Relation> getRelations(Long id) {
        EntityManager em = getEmf().createEntityManager();
        List<Relation> list = em.createQuery("select r from " + Relation.class.getSimpleName() +
                " r where r.first.id = " + id +
                " or r.other.id = " + id)
                .getResultList();
        em.close();
        return list;
    }

    @Override
    public Relation getRelation(Long firstId, Long otherId) {
        EntityManager em = getEmf().createEntityManager();
        Relation r = (Relation) em.createQuery("select r from " + Relation.class.getSimpleName() +
                " r where ( r.first.id = " + firstId +
                " and r.other.id = " + otherId + " ) or " +
                "( r.first.id = " + otherId +
                " and r.other.id = " + firstId + " )")
                .getSingleResult();
        em.close();
        return r;
    }
}
