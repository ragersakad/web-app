package edu.ragersakad.familytree.db.DAOs;

import edu.ragersakad.familytree.core.Person;
import edu.ragersakad.familytree.core.Relation;
import edu.ragersakad.familytree.db.utils.IDAO;
import java.util.List;

/**
 * Methods supported by Relation container.
 * 
 * @author Cecilia Schmidt
 */
public interface IRelationDAO extends IDAO<Relation, Long> {

    /**
     * Adds a relation between two people. The relation may be directed in the
     * form; "first is type to other".
     *
     * @param first
     * @param other
     * @param type the relation type
     */
    public void addRelation(Person first, Person other, Relation.RelationType type);

    public void addMotherChild(Person mother, Person child);

    public void addFatherChild(Person father, Person child);

    /**
     * Get all relations for person with given id.
     *
     * @param id
     * @return
     */
    public List<Relation> getRelations(Long id);

    /**
     * Get the relation between two persons.
     *
     * @param firstId Id of first person.
     * @param otherId Id of second person.
     * @return The relation, null if not found.
     */
    public Relation getRelation(Long firstId, Long otherId);
}
