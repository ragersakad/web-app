package edu.ragersakad.familytree.db.DAOs;

import edu.ragersakad.familytree.core.Person;
import edu.ragersakad.familytree.db.utils.AbstractDAO;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * A collection class for Person class.
 * 
 * @author Cecilia Schmidt
 */
public final class PersonDAO extends AbstractDAO<Person, Long>
        implements IPersonDAO {
    
    /**
     * A constructor that initializes a database for the Person class.
     * 
     * @param puName The persistence unit name
     */
    public PersonDAO(String puName) {
        super(Person.class, puName);
    }

    // Factory method
    public static IPersonDAO newInstance(String puName) {
        return new PersonDAO(puName);
    }
    
    @Override
    public List<Person> getByName(String name) {
        EntityManager em = getEmf().createEntityManager();
        List<Person> list = em.createQuery("select p from " + Person.class.getSimpleName() +
                " p where p.firstName = '" + name +
                "' or p.lastName = '" + name + "'")
                .getResultList();
        em.close();
        return list;
    }
}
