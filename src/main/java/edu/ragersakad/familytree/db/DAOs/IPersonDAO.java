package edu.ragersakad.familytree.db.DAOs;

import edu.ragersakad.familytree.core.Person;
import edu.ragersakad.familytree.db.utils.IDAO;
import java.util.List;

/**
 * Methods supported by the Person container. 
 * 
 * @author Cecilia Schmidt
 */
public interface IPersonDAO extends IDAO<Person, Long> {
    
    public List<Person> getByName(String name);
}
