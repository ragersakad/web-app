package edu.ragersakad.familytree.core;

import edu.ragersakad.familytree.db.utils.AbstractEntity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;

/**
 * A class that represents a person.
 * 
 * @author Cecilia Schmidt
 */
@Entity
public class Person extends AbstractEntity implements Serializable {
    
    public enum Gender {
        FEMALE,
        MALE,
    }
    
    @Column(nullable = false)
    private String  firstName, lastName;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender  gender;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date    birthDate, deathDate;

    public Person() {
    }
    
    /**
     * Creates a person without any of its relations.
     */
    public Person(String firstName, String lastName, Gender gender, Date birthDate, Date deathDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }
    
    public Person(Long id, String firstName, String lastName, Gender gender, Date birthDate, Date deathDate) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }
    
    public String getName() {
        return firstName + " " + lastName;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public Gender getGender() {
        return gender;
    }
    
    public Date getBirthDate() {
        return birthDate;
    }
    
    public Date getDeathDate() {
        return deathDate;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public void setGender(Gender gender) {
        this.gender = gender;
    }
    
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    public void setDeathDate(Date deathDate) {
        this.deathDate = deathDate;
    }
    
    @Override
    public String toString() {
        return "Person{" + "id=" + getId() + 
                ", \n\tfirstName=" + firstName + ", lastName=" + lastName + 
                ", \n\tgender=" + gender.toString() +
                ", \n\tbirthDate=" + birthDate +", deathDate=" + deathDate + '}';
    }
}
