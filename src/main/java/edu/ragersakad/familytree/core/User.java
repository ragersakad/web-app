package edu.ragersakad.familytree.core;

import edu.ragersakad.familytree.db.utils.AbstractEntity;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: guillermo
 * Date: 9/29/13
 * Time: 22:25
 *
 * This class identifies the users in the application
 */

@Entity
@Table(name = "USERS")
public class User extends AbstractEntity{

    @Column(unique = true,nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    @OneToOne
    private Person rootPerson;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Encrypts given password (see encryptString()) and assigns it to this.
     * If encryption failed, the string will be assigned to this anyways.
     */
    public void setPassword(String password) {
        try {
            this.password = User.encryptString(password);

        } catch (Exception e) {
            this.password = password;
        }
    }

    /**
     * Checks if enteredPassword matches the user's.
     */
    public boolean checkPassword(String enteredPassword) {
        try {
            return User.encryptString(enteredPassword).equals(this.password);

        } catch (Exception e) {
            /*
             * In case the previous sentence fails, the password shouldn't be
             * encrypted in the first place.
             */
            return enteredPassword.equals(this.password);
        }
    }
    
    public Person getRootPerson() {
        return rootPerson;
    }
    
    public void setRootPerson(Person p) {
        this.rootPerson = p;
    }

    /**
     * Encrypts a string with SHA-256.
     *
     * Returns the hexadecimal representation of the "digested" string.
     * NoSuchAlgorithmException if SHA-256 isn't available.
     * UnsupportedEncodingException if it couldn't "digest" the string.
     */
    public static String encryptString(String enteredPassword) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md;
        md = MessageDigest.getInstance("SHA-256");
        md.update(enteredPassword.getBytes("UTF-8"));
        byte byteData[] = md.digest();
        // convert the byte to hex format method 1
        StringBuilder sb = new StringBuilder();

        for (byte aByteData : byteData) {
            sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}
