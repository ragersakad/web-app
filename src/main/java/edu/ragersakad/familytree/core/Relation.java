package edu.ragersakad.familytree.core;

import edu.ragersakad.familytree.db.utils.AbstractEntity;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

/**
 * Class to handle a relation between two people.
 * 
 * @author Cecilia Schmidt
 */
@Entity
public class Relation extends AbstractEntity implements Serializable {
    /**
     * Used to specify type of relation.
     */
    public enum RelationType {
        FATHER("father"),
        MOTHER("mother"),
        SPOUSE("spouse"),
        CHILD("child");  // NOTE: should not be used with Relation constructor!
        
        private String name;

        RelationType(String name) {
            this.name = name;
        }

        /**
         * Gets the name of the enum.
         *
         * @return the enums name
         */
        public String getName() {
            return this.name;
        }

        /**
         * Converts a string to one of RelationType's enum values.
         *
         * @param text String to convert.
         * @return the RelationType enum, null if no value matched.
         */
        public static RelationType fromString(String text) {
            if (text != null) {
                for (RelationType r : RelationType.values()) {
                    if (text.equalsIgnoreCase(r.name)) {
                        return r;
                    }
                }
            }
            return null;
        }
    }
    
    @OneToOne
    private Person first, other;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private RelationType type;
    
    public Relation() {
    }
    
    /**
     * This constructor shouldn't be used with Type.CHILD. 
     * Use parental (mother, father) types instead!
     * 
     * TODO is there any way to prohibit Type.CHILD?
     * 
     * @param first parent, spouse
     * @param other child, spouse
     * @param type 
     */
    public Relation(Person first, Person other, RelationType type) {
        this.first = first;
        this.other = other;
        this.type = type;
    }
    
    public Relation(Long id, Person first, Person other, RelationType type) {
        super(id);
        this.first = first;
        this.other = other;
        this.type = type;
    }
    
    public Person getFirst() {
        return first;
    }
    
    public Person getOther() {
        return other;
    }
    
    public Relation.RelationType getType() {
        return type;
    }
    
    @Override
    public String toString() {
        return "Relation{" + "id=" + getId() + 
                ", \n\tfirst Person=" + first.getId() + " " + first.getName() + 
                ", \n\tother Person=" + other.getId() + " " + other.getName() + '}';
    }
}
