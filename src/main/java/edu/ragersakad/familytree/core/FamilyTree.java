package edu.ragersakad.familytree.core;

import edu.ragersakad.familytree.db.DAOs.IPersonDAO;
import edu.ragersakad.familytree.db.DAOs.IRelationDAO;
import edu.ragersakad.familytree.db.DAOs.PersonDAO;
import edu.ragersakad.familytree.db.DAOs.RelationDAO;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;

/**
 * This class is a wrapper to all the underlying classes in the family tree.
 * 
 * @author Cecilia Schmidt
 */
public class FamilyTree implements IFamilyTree {
    
    private final IPersonDAO personDB;
    private final IRelationDAO relationDB;

    public FamilyTree(String persistenceUnitName) {
        this.personDB = PersonDAO.newInstance(persistenceUnitName);
        this.relationDB = RelationDAO.newInstance(persistenceUnitName);
        
        Logger.getAnonymousLogger().log(Level.INFO, "FamilyTree is up {0}", this.hashCode());
    }

    @Override
    public IPersonDAO getPersonDAO() {
        return personDB;
    }

    @Override
    public IRelationDAO getRelationDAO() {
        return relationDB;
    }

    @Override
    public Person addPerson(String firstName, String lastName, Person.Gender gender, Date birthDate, Date deathDate) {
        Person p = new Person(firstName, lastName, gender, birthDate, deathDate);
        personDB.add(p);
        return p;
    }

    @Override
    public Person updatePerson(Long id, String firstName, String lastName, Person.Gender gender, Date birthDate, Date deathDate) {
        Person p = new Person(id, firstName, lastName, gender, birthDate, deathDate);
        return personDB.update(p);
    }

    @Override
    public void deletePerson(Long id) {
        List<Relation> relations = relationDB.getRelations(id);
        for (Relation r : relations) {
            relationDB.remove(r.getId());
        }
        personDB.remove(id);
    }

    @Override
    public Person getById(Long id) {
        return personDB.find(id);
    }

    @Override
    public List<Person> getByName(String name) {
        return personDB.getByName(name);
    }

    @Override
    public void createRelation(Long srcId, Long dstId, Relation.RelationType type) {
        Person p1 = personDB.find(srcId);
        Person p2 = personDB.find(dstId);
        Relation r = new Relation(p1, p2, type);
        relationDB.add(r);
    }

    @Override
    public void deleteRelation(Long srcId, Long dstId) {
        Relation r = relationDB.getRelation(srcId, dstId);
        relationDB.remove(r.getId());
    }

    @Override
    public List<Relation> getRelations(Long id) {
        return relationDB.getRelations(id);
    }

    @Override
    public Person getRandomPerson() {
        List<Person> all = personDB.getAll();
        if (all.isEmpty()) {
            return null;
        }

        Random randomGenerator = new Random();
        return all.get(randomGenerator.nextInt(all.size()));
    }
}
