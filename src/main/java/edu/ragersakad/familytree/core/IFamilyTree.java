package edu.ragersakad.familytree.core;

import edu.ragersakad.familytree.db.DAOs.IPersonDAO;
import edu.ragersakad.familytree.db.DAOs.IRelationDAO;

import java.util.Date;
import java.util.List;

/**
 * @author Cecilia Schmidt
 */
public interface IFamilyTree {

    /**
     * Get the data access object for all persons.
     *
     * @return The data access object for all persons.
     */
    public IPersonDAO getPersonDAO();

    /**
     * Get the data access object for all relations.
     *
     * @return The data access object for all relations.
     */
    public IRelationDAO getRelationDAO();

    
    // PeopleResources
    /**
     * Adds a person with the given details.
     *
     * @param firstName
     * @param lastName
     * @param gender
     * @param birthDate
     * @param deathDate
     * @return
     */
    public Person addPerson(String firstName, String lastName,
            Person.Gender gender, Date birthDate, Date deathDate);

    /**
     * Updates an existing person with the given details.
     *
     * @param id Id of person to update.
     * @param firstName
     * @param lastName
     * @param gender
     * @param birthDate
     * @param deathDate
     * @return The updated person.
     */
    public Person updatePerson(Long id, String firstName, String lastName,
            Person.Gender gender, Date birthDate, Date deathDate);

    /**
     * Deletes the person with given id.
     *
     * @param id Id of person to be deleted.
     */
    public void deletePerson(Long id);

    /**
     * Gets a person with the given id.
     *
     * @param id Id of the person to get.
     * @return The person. Null if no id matches.
     */
    public Person getById(Long id);

    /**
     * Gets all persons with matching names.
     *
     * @param name The name to match persons names with.
     * @return Persons with matching names.
     */
    public List<Person> getByName(String name);

    
    // ConnectionsResources
    /**
     * Creates a relation between two persons.
     *
     * @param srcId Id of first person.
     * @param dstId Id of second person.
     * @param type relation type note: relations can be directed ex; srcId is
     * type to dstId first person is father to second person
     */
    public void createRelation(Long srcId, Long dstId, Relation.RelationType type);

    /**
     * Deletes a relation between two persons.
     *
     * @param srcId Id of first person.
     * @param dstId Id of second person.
     */
    public void deleteRelation(Long srcId, Long dstId);

    
    // TreeResources
    /**
     * Get all connections for a Person.
     *
     * @param id Id of a Person
     * @return All connections for Person
     */
    public List<Relation> getRelations(Long id);

    /**
     * Get a random person.
     *
     * @return a random Person, null if database is empty
     */
    public Person getRandomPerson();
}
