
*** Family Tree Builder ***
Created by Ragersakad, group 13

The documentation for the project is hosted as a wiki at:
https://bitbucket.org/ragersakad/web-app/wiki/Home

The documentation for the API is at:
http://docs.familytree.apiary.io/

A live demo of the website at:
http://familytreebuilder.tk

********

For the git inspection at least the following flags should be used:

-x src/main/webapp/lib/*
-x src/main/webapp/js/test/qunit.js
-x src/main/webapp/js/gui/jquery.graphCreator.js